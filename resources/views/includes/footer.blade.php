<footer id="footer">
	<div class="row" style="background:#012530">
		<div class="col-md-2 col-sm-4 col-xs-5">
			<a href="{{ url($prev) }}"><button class="btn btn-option">{{ trans('lang.prev') }}</button></a>
		</div>
		<div class="col-md-8 col-sm-4 col-xs-2"></div>
		<div class="col-md-2 col-sm-4 col-xs-5">
			<button id="next" class="btn btn-option" value="{{ $value }}">{{ trans('lang.next') }}</button>
		</div>
	</div>
</footer>	