<div class="col-md-2">
	<div class="sidebarborder">
	<label>SHIPPING INFORMATION</label>
	<br>
	@if (Session::has(['size','weight','status']))
			<br><label>Packing status</label>
	    <br>Size : {{ Session::get('size') }} cm<sup>3</sup>
	    <br>Weight : {{ Session::get('weight') }} kg
	    <br>Packed : 
	    @if(Session::get('status')=="0")
	      &#x2718;
	    @else
	      &#x2714;
	    @endif
	 @else
	    -
	 @endif

	<br>
	<br><label>Items info</label>
	<br>
	@if (Session::has(['totalqty','grandtotal']))
	    Quantity : {{ Session::get('totalqty') }}
	    <br>Grand total : ¥{{ Session::get('grandtotal') }}
	@else
		-
	@endif

	<br>
	<br><label>Address info</label>
	<br>
	@if (Session::has(['deliveryselection']))
		<?php $ds = Session::get('deliveryselection'); ?>
		@if($ds=="0")
			{{ Session::get('name') }} ({{ Session::get('email') }})
			<br>{{ Session::get('address') }}
			<br>{{ Session::get('prefecture') }}
			<br>{{ Session::get('postcode') }}
			<br>{{ Session::get('phone') }}
		@elseif($ds=="1") 
			{{ Session::get('airport_name') }}
			<br>{{ Session::get('airport_address') }}
			<br>{{ Session::get('airport_tel') }}
			<br>{{ Session::get('airport_code') }}
			<br>Flight no : {{ Session::get('flight_no') }}
			<br>Departure Time : {{ Session::get('departure_date').Session::get('time') }}
		@endif
	@else
		-
	@endif

	<br>
	<br><label>Payment method</label>
	<br>
	@if(Session::has('payment_method'))
		{{ Session::get('payment_method') }}
	@else
		-
	@endif
	
	
	</div> <!-- .sidebarborder -->
</div><!-- .col-md-2 -->
