<style>
.progress-step-wrapper{
  padding: 50px 0 20px 0;
}
ul.progress-step{
  text-align: center;
}
ul.progress-step li{
  display: inline-block;
  position: relative;
  margin: 0 30px;
  font-size: 18px;
  min-width: 90px;
}
ul.progress-step li::before{
  position: absolute;
  top: -50px;
  left: 0;
  right: 0;
  margin: 0 auto;
  width: 50px;
  height: 50px;
  border: 1px solid green;
  border-radius: 50%;
  padding: 13px;
  background-color: rgb(255,255,255);
}

ul.progress-step li.step-1::before{
  content: '1';
}
ul.progress-step li.step-2::before{
  content: '2';
}
ul.progress-step li.step-3::before{
  content: '3';
}
ul.progress-step li.step-4::before{
  content: '4';
}
ul.progress-step li.step-5::before{
  content: '5';
}

ul.progress-step li::after{
  content: '';
  position: absolute;
  width: 110px;
  height: 2px;
  background-color: green;
  top: -25px;
}
ul.progress-step li.step-1::after{
  left: 70px;
}
ul.progress-step li.step-2::after{
  left: 70px;
}
ul.progress-step li.step-3::after{
  left: 70px;
}
ul.progress-step li.step-4::after{
  left: 70px;
}
ul.progress-step li.step-5::after{
  display: none;
}
ul.progress-step li.current::before{
  color: rgb(255, 255, 255);
  background-color: green;
}
</style>
<input type="hidden" id="id-current" value="{{ $current }}">
<div class="progress-step-wrapper">
  <ul class="progress-step">
    <li id="1" class="step-1">rules</li>
    <li id="2" class="step-2">size</li>
    <li id="3" class="step-3">items</li>
    <li id="4" class="step-4">destination</li>
    <li id="5" class="step-5">payment</li>
  </ul>
</div>

<script type="text/javascript">
var idcurrent = $('#id-current').val();

$(function(){
  $('ul.progress-step li#'+idcurrent).addClass('current');
})
</script>
