@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h2><center>Enter your credit card info.</center></h2>
				<form class="form-horizontal" action={{ url('/get_cc_info') }} method="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <div class="form-group">
					    <label for="" class="col-sm-3 control-label">Name</label>
					    <div class="col-sm-6">
					      <input type="text" name="name" class="form-control">
					    </div><!-- .col-sm-6 -->
					  </div><!-- .form-group -->
					  <div class="form-group">
					    <label for="" class="col-sm-3 control-label">Card number</label>
					    <div class="col-sm-6">
					      <input type="text" name="cardnumber" class="form-control">
					    </div><!-- .col-sm-6 -->
					  </div><!-- .form-group -->
					  <div class="form-group">
					    <label for="" class="col-sm-3 control-label">Expire date</label>
					    <div class="col-sm-6">
					      <input type="text" name="expiredate" class="form-control">
					    </div><!-- .col-sm-6 -->
					  </div><!-- .form-group -->
					  <div class="form-group">
					    <label for="" class="col-sm-3 control-label">CVCC Number</label>
					    <div class="col-sm-6">
					      <input type="text" name="cvcc" class="form-control">
					    </div><!-- .col-sm-6 -->
					  </div><!-- .form-group -->
					  <div class="form-group">
					  		<button type="submit" class="btn btn-info center-block">Next</button>
					  </div><!-- .form-group -->
				</form>
			</div><!-- .col-md-8 col-md-offset-2 -->
			@include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container-fluid -->

@extends('includes.footer', ['prev' => 'domestic_shipping','next' => 'payment_complete', 'value' => ''])
@endsection