@extends('layouts.vue_template')

@section('content')
<selectshop :lang="{{ json_encode(__('lang')) }}" :shops="{{ json_encode($shops) }}" :url="'{{ url('/') }}'"></selectshop>
@endsection