@extends('layouts.maintemplate', ['filename' => 'content_item'])

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2><center>Enter the content items, unit price, and amount.</center></h2>
			<form id="form" class="form-inline" action={{ url('/int_get_total') }} method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				        <div class="col-md-12">
	                <div class="row">
		                  <div class="col-md-3 col-md-offset-1">
		                    <label class="text-center">Content Items</label>
		                  </div><!-- .form-group col-md-3 -->
		                  <div class="form-group col-md-2">
		                    <label>Unit Price</label>
		                  </div><!-- .form-group col-md-3 -->
		                  <div class="form-group col-md-1">
		                    <label>Pieces</label>
		                  </div><!-- .form-group col-md-1 -->
		                  <div class="form-group col-md-4 col-md-offset-1">
		                    <label class="text-center">Total price</label>
		                  </div><!-- .form-group col-md-3 -->
	                </div><!-- .row -->
                </div><!-- .col-md-12 -->
                <div id="field">
                @for($i=0;$i<6;$i++)
                <div class="col-md-12 field-input" style="margin:10px;" id="field-{{ $i }}">
                  <div class="form-group col-md-3">
                    <select class="form-control" id="content_items" name="content_items[]">
                      @foreach($contents as $content)
                      <option>{{ $content->name }}</option>
                      @endforeach
                    </select>
                  </div><!-- .col-md-12 -->
                  
                  <div class="form-group col-md-3">
                    <!--<span class="input-group-addon">¥</span>-->
                    <input type="number" class="form-control" name="prices[]" aria-label="Amount (to the nearest yen)" onchange="sum()">
                    <!--<span class="input-group-addon">.00</span>-->
                  </div><!-- .form-group col-md-3 -->
                  
                  <div class="form-group col-md-1">
                    <select class="form-control" name="pieces[]" onchange="sum()">
                      @for($j=0;$j<=99;$j++)
                      <option value={{ $j }}>{{ $j }}</option>
                      @endfor
                    </select>
                  </div><!-- .form-group col-md-1 -->

                  <div class="form-group col-md-3">
                    <input type="text" name="totals[]" class="form-control total" readonly onchange="sum()">
                  </div><!-- .form-group col-md-3 -->

                  <div class="form-group col-md-2">
                  	<button type="button" id="remove" name="remove" class="btn btn-danger form-control btn-remove" data-field="{{$i}}">-</button>
                  </div><!-- .form-group col-md-2 -->
                </div><!-- .col-md-12 field-input -->
                @endfor
                </div><!-- .field -->

                <div class="col-md-12" style="margin:10px;">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
                      <label class="col-form-label">Grand total price</label>
                    </div><!-- .col-md-4 -->
                    <div class="form-group col-md-3">
                      <input type="text" id="grandtotal" name="grandtotal" class="form-control grandtotal" readonly>
                    </div><!-- .form-group col-md-3 -->
                    <div class="form-group col-md-2">
                      <button type="button" id="add" name="add" class="btn btn-primary form-control btn-add">+</button>
                    </div><!-- .form-group col-md-2 -->
                 </div><!-- .col-md-12 -->
			</form>
		</div><!-- .col-md-8 -->
    @include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container-fluid -->

<script src="js/custom.js"></script>
<script>

    $(function(){
     
        var countfield = $(".field-input").length;

        var newfield = '<div class="col-md-12 field-input" style="margin:10px;" id="field-'+countfield+'"><div class="form-group col-md-3"><select class="form-control" id="content_items" name="content_items[]">@foreach($contents as $content)<option>{{ $content->name }}</option>@endforeach</select></div><div class="form-group col-md-3"><input type="number" id="price-'+countfield+'" class="form-control price-input" name="prices[]" aria-label="Amount (to the nearest yen)" onchange="sum()"></div><div class="form-group col-md-1"><select id="piece-'+countfield+'}}" class="form-control piece-option" name="pieces[]" onchange="sum()">@for($j=0;$j<=99;$j++)<option value={{ $j }}>{{ $j }}</option>@endfor</select></div><div class="form-group col-md-3"><input type="text" name="totals[]" id="total-'+countfield+'" class="form-control total" readonly></div><div class="form-group col-md-2"><button type="button" id="remove" name="remove" class="btn btn-danger form-control btn-remove" data-field="'+countfield+'"}}">-</button></div></div>';

          $("#add").click(function(){
              //console.log(countfield);
              countfield += 1;
              $("#field").append(newfield);
          });

          $("body").on("click",".btn-remove", function(){
              countfield = $(".field-input").length;
              if(countfield>1)
              {
                  var id = $(this).data("field");
                  $("#field-"+id).remove();
              }
              else
              {
                  //alert("At least 1 field exist");
              }
          });

          $(".piece-option").bind('change',function(){
              sum();
          });
    });

    function sum()
    {
        Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
            var n = this,
                decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
                decSeparator = decSeparator == undefined ? "." : decSeparator,
                thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
                sign = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
        };

        var grandtotal = 0;
        
        var prices = document.getElementsByName('prices[]');
        var pieces = document.getElementsByName('pieces[]');
        var totals = document.getElementsByName('totals[]');

        for (var i=0;i<$('.total').length;i++) {
            var price = parseInt(prices[i].value) || 0;
            var piece = parseInt(pieces[i].value) || 0;
            
            totalvalue = price * piece;
            totals[i].value = totalvalue.toLocaleString();
            grandtotal += totalvalue;
        }
        document.getElementById('grandtotal').value = grandtotal.toLocaleString();
    }
</script>

@include('includes.footer', ['prev' => 'size_int','next' => 'international_destination_address', 'value' => 'step4'])
@endsection