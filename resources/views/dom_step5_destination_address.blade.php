@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')

<div class="container-fluid">
	<div class="row">
		@include('includes.progress-step', ['current' => '4'])
		<div class="col-md-8 col-md-offset-2">
			<h2><center>Enter the domestic destination address.</center></h2>
			<form id="form" class="form-horizontal" action={{ url('/dom_get_address_data') }} method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <div class="form-group-inline selectdelivery col-md-offset-4">	
			    		<div class="form-group selectdelivery center-block">
							<div class="radio-inline col-md-3 col-md-6 col-xs-6">
								<label class="" style="min-height: 78px;">
								<input type="radio" name="deliveryselection" class="deliveryselection" value="0" data-switchon="0">
								Domestic Address
								</label>
							</div><!-- .radio-inline col-md-3 col-md-6 col-xs-6 -->
							<div class="radio-inline col-md-3 col-md-6 col-xs-6">
								<label style="min-height: 78px;">
									<input type="radio" name="deliveryselection" class="deliveryselection" value="1" checked="checked" data-switchon="1">
									Domestic Airport
								</label>
							</div><!-- .radio-inline col-md-3 col-md-6 col-xs-6 -->
						</div><!-- .form-group selectdelivery -->
					  </div><!-- .form-group-inline -->
				  		<div class="domestic-address-form" style="display: none;">
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Destination Prefecture</label>
							    <div class="col-md-6">
							      <select class="form-control" name="prefectureid" id="airport">
							      	@foreach($prefectures as $prefecture)
								    	<option value={{ $prefecture->id }}>{{ $prefecture->name }}</option>
								    @endforeach
								  </select>
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Destination Address</label>
							    <div class="col-md-6">
							      <input type="text" name="address" class="form-control">
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Destination post code</label>
							    <div class="col-md-6">
							      <input type="text" name="postcode" class="form-control">
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Customer Tel</label>
							    <div class="col-md-6">
							      <input type="text" name="phone" class="form-control">
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Customer Name</label>
							    <div class="col-md-6">
							      <input type="text" name="name" class="form-control">
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Email</label>
							    <div class="col-md-6">
							      <input type="text" name="email" class="form-control">
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Delivery date</label>
							    <div class="col-md-6">
							      <input type="text" name="delivery-date" class="form-control" value="<?php echo date('Y-m-d', strtotime('+2 day')) ?>" readonly>
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
						  </div><!-- .domestic-address-form -->

						  <div class="domestic-airport-form" style="display: none;">
						  	<div class="col-md-6">
						  		<div id="map"></div>
						  	</div><!-- .col-md-6 -->
						    <div class="form-group col-md-6">
						    	@foreach($airports as $airport)
						      	<button type="button" name="ap" value="{{ $airport->id }}" class="btn btn-lg btn-primary btn-block ap">{{ $airport->name }}</button>
						      	@endforeach
						    </div><!-- .form-group col-md-6 -->
						  	<div class="col-md-12" style="margin-top: 30px;">
					  			<div class="col-md-8">
					  				<label>Airport Information</label>
					  			</div><!-- .col-md-8 -->
							    <div class="col-md-8">
							      <input type="text" readonly class="form-control-plaintext" id="airport-name" name="airport_name" value="" style="border: 0px; width:100%;">
							    </div><!-- .col-md-8 -->
							  	<div class="col-md-8">
							      <input type="text" readonly class="form-control-plaintext" id="airport-address" name="airport_address" value="" style="border: 0px; width:100%;">
							    </div><!-- .col-md-8 -->
							    <div class="col-md-8">
							      <input type="text" readonly class="form-control-plaintext" id="airport-tel" name="airport_tel" value="" style="border: 0px; width:100%;">
							    </div><!-- .col-md-8 -->
							    <div class="col-md-6">
							      <input type="text" readonly class="form-control-plaintext" id="airport-code" name="airport_code" value="" style="border: 0px; width:100%;">
							    </div><!-- .col-md-8 -->
						  	</div><!-- .col-md-12 -->
					  		<div class="col-md-12">
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Flight No.</label>
							    <div class="col-md-6">
							      <input type="text" name="flight_no" class="form-control">
							    </div><!-- .col-md-8 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Departure Date</label>
							    <div class="col-md-6">
							      <input type="date" name="departure_date" class="form-control">
							    </div><!-- .col-md-8 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Departure Time</label>
							    <div class="col-md-6">
							      <input type="time" name="time" class="form-control">
							    </div><!-- .col-md-8 -->
							  </div><!-- .form-group -->
							  <div class="form-group">
							    <label for="" class="col-md-4 control-label">Delivery date</label>
							    <div class="col-md-6">
							      <input type="text" name="delivery-date" class="form-control" value="<?php echo date('Y-m-d', strtotime('+3 day')) ?>" readonly>
							    </div><!-- .col-md-6 -->
							  </div><!-- .form-group -->
					  		</div><!-- .col-md-12 -->
						  </div><!-- .domestic-airport-form -->
			</form>
		</div><!-- .col-md-8 -->
		@include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container-fluid -->

<style>
#map {
  height: 400px;
  width: 100%;
 }

.btn-primary:active:focus {
  color: red; 
  background-color: #161617; 
  border-color: #494F57;
}
</style>

<script src="js/custom.js"></script>
<script>

	var airports = <?php echo $airports ?>;

  function initMap() {
  	
    var coordinate = {lat: 35.4875472, lng: 137.5602275};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 6,
      center: coordinate
    });

    var image = {
	    url: "images/airport.png",
	    // This marker is 20 pixels wide by 32 pixels high.
	    scaledSize: new google.maps.Size(40, 40),
	    origin: new google.maps.Point(0,0), // origin
    	anchor: new google.maps.Point(0, 0) // anchor
  	};
    
  	var infowindow = new google.maps.InfoWindow();

    for(var i=0;i<airports.length;i++)
    {
    	var airport_coordinate = {lat: airports[i].latitude, lng: airports[i].longitude};
	    var marker = new google.maps.Marker({
	      position: airport_coordinate,
	      title: airports[i].name,
	      map: map,
	      icon: image
	    });

	    google.maps.event.addListener(marker, 'click', (function(marker, i) {
	    	return function() {
		    	updateAirportInfo(airports[i].name, airports[i].address, airports[i].tel, airports[i].postcode);
		    }
		})(marker, i));
	}	
  }
  
  function updateAirportInfo(name, address, tel, code)
  {
  		var airport_name = document.getElementById('airport-name');
  		var airport_address = document.getElementById('airport-address');
  		var airport_tel = document.getElementById('airport-tel');
  		var airport_code = document.getElementById('airport-code');
  		airport_name.value = name;
  		airport_address.value = address;
  		airport_tel.value = tel;
  		airport_code.value = code;
  }
  
/*
  	$(".ap").click(function() {
	  	$( "ap" ).each(function( i ) {
		    if ( this.style.color !== "blue" ) {
		      this.style.color = "blue";
		    } else {
		      this.style.color = "";
		    }
  		});

	});
	*/
	/*
  $(".ap").click(function(e) {
  		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

	    e.preventDefault();
	    $.ajax({
	        type: "POST",
	        url: "{{ url('/getAirportCoordinate') }}",
	        data: { 
	            id: $(this).val()
	        },
	        success: function(data) {
	            var coordinate = {lat: data[0].latitude, lng: data[0].longitude};
	            var map = new google.maps.Map(document.getElementById('map'), {
			      zoom: 9,
			      center: coordinate
			    });
	            var marker = new google.maps.Marker({
			      position: coordinate,
			      map: map
			    });
			    updateAirportInfo(data[0].name, data[0].address, data[0].tel, data[0].postcode);
	        },
	        error: function(ts) {
	            //console.log('Error:', data);
	            console.log(ts.responseText);
	        }
	    });
	});
	*/
	$(".ap").click(function(e) {
  		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

	    e.preventDefault();
	    $.ajax({
	        type: "POST",
	        url: "{{ url('/getAirportCoordinate') }}",
	        data: { 
	            id: $(this).val()
	        },
	        success: function(data) {
			    updateAirportInfo(data[0].name, data[0].address, data[0].tel, data[0].postcode);
	        },
	        error: function(ts) {
	            //console.log('Error:', data);
	            console.log(ts.responseText);
	        }
	    });
	});
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWxgd1LEqFR9sYXYDuJlE5IG8X6jy0DSY&callback=initMap"></script>

@include('includes.footer', ['prev' => 'domestic_content_item','next' => 'domestic_shipping', 'value' => 'step5'])

@endsection