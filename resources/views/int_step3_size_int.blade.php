@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12">
						<h2><center>Enter the total size and weight for overseas shipping.</center></h2>
						<br>
						<form id="form" class="form-horizontal" action={{ url('/get_size_int') }} method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
						  		<label for="packing" class="col-sm-4 control-label">Packing &amp; cushioning status</label>
						    	<div class="col-sm-8">
						    		<div class="row">
									<div class="col-md-12">
										<div class="form-group selectdelivery">
											<div class="radio col-md-3 col-sm-6 col-xs-6">
												<label class="" style="min-height: 78px;">
													<input type="radio" name="deliveryselection" class="deliveryselection radio col-md-3 col-sm-6 col-xs-6" value="1" checked="" data-switchon="0">Completed
												</label>
											</div><!-- .radio col-md-3 col-sm-6 col-xs-6 -->
											<div class="radio col-md-3 col-sm-6 col-xs-6">
												<label style="min-height: 78px;" class="checked">
													<input type="radio" name="deliveryselection" class="deliveryselection radio col-md-3 col-sm-6 col-xs-6" value="0" data-switchon="1">
													Not yet (Order it)
													<div style="color:blue;">+packing cost 500 JPY
													+packing material weight 1.0kg</div>
												</label>
											</div><!-- .radio col-md-3 col-sm-6 col-xs-6 -->
										</div><!-- .form-group selectdelivery -->
						    		</div><!-- .col-md-12 -->
						  			</div><!-- .row -->
						  		</div><!-- .col-sm-8 -->
						  	</div><!-- .form-group -->
						  <!--<div class="delivery-request-data" style="display: none;">-->
						  <div class="form-group">
						    <label for="Size" class="col-sm-4 control-label">Size (H+W+L = total cm)</label>
						    <div class="col-sm-6">
						      <input type="text" id="size" name="size" class="form-control">
						    </div><!-- .col-sm-6 -->
						  </div><!-- .form-group -->
						  <div class="form-group">
						    <label for="Weight" class="col-sm-4 control-label">Weight (kg)</label>
						    <div class="col-sm-6">
						      <input type="text" id="weight" name="weight" class="form-control">
						    </div><!-- .col-sm-6 -->
						  </div><!-- .form-group -->  
						  <!--</div>-->
						</form>
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .col-md-8 -->
			<div class="col-md-2"></div>
	</div><!-- .row -->
</div><!-- .container-fluid -->

<script src="js/custom.js"></script>

@include('includes.footer', ['prev' => 'ems','next' => 'international_content_item', 'value' => 'step3'])
@endsection