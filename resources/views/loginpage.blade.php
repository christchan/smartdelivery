@extends('layouts.maintemplate', ['filename' => 'loginpage'])

@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="loginpage">
			<div class="col-md-4 col-md-offset-4">
				<div class="row">
					<div class="col-md-12">
						<form id="form" action="" class="form-horizontal" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<h1 class="text-center">{{ trans('lang.appname') }}</h1>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input type="text" id="id" name="email" class="form-control" placeholder="{{ trans('lang.login_id') }}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input type="password" id="password" name="password" class="form-control" placeholder="{{ trans('lang.password') }}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input type="checkbox" name="" value="" checked> {{ trans('lang.remember_me') }}<br>
								</div>
								<div class="col-md-12">
									<span style="font-style:italic;font-size:0.9em">You don't have to enter your password for second time</span>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="footer">
	<div class="row" style="background:#012530">
		<div class="col-md-10 col-sm-10 col-xs-6"></div>
		<div class="col-md-2 col-sm-2 col-xs-6">
			<button id="login" class="btn btn-option">{{ trans('lang.login') }}</button>
		</div>
	</div>
</div>

<script src="js/custom.js"></script>

@endsection