@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2><center>Enter the destination address.</center></h2>
			<form id="form" class="form-horizontal" action={{ url('/int_get_address_data') }} method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">  		
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">Country</label>
				    <div class="col-md-6">
				      <select class="form-control" name="prefectureid" id="airport">
				      	@foreach($countries as $country)
					    	<option value={{ $country->id }}>{{ $country->name }}</option>
					    @endforeach
					  </select>
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">State</label>
				    <div class="col-md-6">
				      <input type="text" name="address" class="form-control">
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">City</label>
				    <div class="col-md-6">
				      <input type="text" name="postcode" class="form-control">
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">Other Address</label>
				    <div class="col-md-6">
				      <input type="text" name="phone" class="form-control">
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">Post code</label>
				    <div class="col-md-6">
				      <input type="text" name="name" class="form-control">
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">Tel</label>
				    <div class="col-md-6">
				      <input type="text" name="email" class="form-control">
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">Name</label>
				    <div class="col-md-6">
				      <input type="text" name="delivery-date" class="form-control">
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="" class="col-md-4 control-label">Email</label>
				    <div class="col-md-6">
				      <input type="text" name="delivery-date" class="form-control">
				    </div><!-- .col-md-6 -->
				  </div><!-- .form-group -->
			</form>
		</div><!-- .col-md-8 -->
		@include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container-fluid -->
<script src="js/custom.js"></script>
@include('includes.footer', ['prev' => 'international_content_item','next' => 'international_shipping', 'value' => 'step5'])
@endsection