@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')

<div class="container">
	<div class="row">
		@include('includes.progress-step', ['current' => '5'])
		<div class="col-md-8 col-md-offset-2">
			<h2><center>Total shipping cost is the below.<br>Select your payment way.</center></h2>

			<div class="row" style="margin-top: 50px;">
				<div class="col-sm-6">
					<h3 class="text-center">Total shipping cost</h3>
				</div><!-- .col-sm-6 -->
				<div class="col-sm-6">
					<h3 class="text-center"><?php echo number_format(Session::get('cost')).'円'; ?></h3>
				</div><!-- .col-sm-6 -->
			</div><!-- .row -->
			
			<form id="form" class="form-horizontal" action={{ url('/dom_getpaymentmethod') }} method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			  	<div class="form-group">
			  		<div class="row">
			  			<label for="payment-method" class="control-label">Payment method</label>
			  		</div><!-- .row -->
			  		<div class="row" style="margin-top: 25px;">
			    		<div class="col-md-3 col-sm-3 col-xs-12 form-check">  
					        <input class="form-check-input payment" type="radio" name="payment" value="Credit Card" checked>
						  	<label class="form-check-label">
						    	Credit Card
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->
			    		<!--
			    		<div class="col-md-3 col-sm-3 col-xs-12 form-check">
					        <input class="form-check-input payment" type="radio" name="payment" value="Wechat">
						  	<label class="form-check-label">
						    	Wechat
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->
			    		<!--<div class="col-md-3 col-sm-3 col-xs-12 form-check">
					        <input class="form-check-input payment" type="radio" name="payment" value="Alipay">
						  	<label class="form-check-label">
						    	Alipay
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->
			    		<!--<div class="col-md-3 col-sm-3 col-xs-12 form-check">
			    			<input class="form-check-input payment" type="radio" name="payment" value="Unionpay">
						  	<label class="form-check-label">
						    	Unionpay
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->	
			    		<input type="hidden" id="stripeToken" name="stripeToken" value="">
			    	</div><!-- .row -->
			    </div><!-- .form-group -->
			</form>
			@if (Session::has(['deliveryselection']))
				<br>
				@if(Session::has('cost') AND Request::path()=="domestic_shipping")
				<button id="pay" class="btn btn-info btn-lg center-block" onclick="loadCheckout()">Pay ¥{{ number_format(Session::get('cost')) }}</button>
				@endif
			@endif
		</div><!-- .col-md-8 -->
		@include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container -->

<script src="js/custom.js"></script>
<!--<script src="https://js.stripe.com/v3/"></script>-->
<script src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">
	function loadCheckout() {
       	var handler = StripeCheckout.configure({
            key:'{{ $pk_key }}',
            image:'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale:'auto',
            token:function(token) {
                //console.log(token);
                $("#stripeToken").val(token.id);
            }
        });
        // Open Checkout with further options:
	      handler.open({
	        name: '{{ config('app.name') }}',
	        description: 'Shipping Payment',
	        currency: '{{ $currency }}',
            locale: 'auto',
            email: '{{ Auth::user()->email }}',
	        amount: {{ Session::get('cost') }}
	      });

	      window.addEventListener('popstate', function() {
            handler.close();
        });
    }
</script>

@include('includes.footer', ['prev' => 'domestic_destination_address','next' => 'creditcard_payment', 'value' => 'step6'])
@endsection