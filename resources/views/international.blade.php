@extends('layouts.vue_template')

@section('content')
<international :shop_id="{{ json_encode($shop_id) }}" :shop_name="{{ json_encode($shop_name) }}" :locale="'{{ app()->getlocale() }}'" :pricetable="{{ json_encode($price_table) }}" :contentitems="{{ json_encode($content_items) }}" :url="'{{ url('/') }}'" :lang="{{ json_encode(__('lang')) }}" :prefectures="{{ json_encode($prefectures) }}" :airports="{{ json_encode($airports) }}" :countries="{{ json_encode($countries) }}" :today="'{{ $today }}'" :stripe="{{ json_encode($stripe) }}" :user="{{ json_encode($user) }}"></international>

@endsection