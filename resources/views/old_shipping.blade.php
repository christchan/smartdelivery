@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2"></div>
			<div class="col-md-8">
				<h2><center>Total shipping cost is the below.<br>Select your payment way.</center></h2>	
				<div class="col-sm-6">
					<h2>
						<center>Total shipping cost</center>
					</h2>
				</div>
				<div class="col-sm-6">
					<h2>
						<?php echo number_format(Session::get('grandtotal')).'円'; ?>
					</h2>
				</div>
				<form class="form-horizontal" action={{ url('/getpaymentmethod') }} method="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				  	<div class="form-group">
				  		<div class="row">
				  			<label for="payment-method" class="control-label">Payment method</label>
				  		</div>
				  		<div class="row">
				    		<div class="col-md-3 col-sm-3 col-xs-12 form-check">  
						        <input class="form-check-input" type="radio" name="payment" value="cc">
							  	<label class="form-check-label">
							    	Credit Card
							  	</label>
				    		</div>
				    		<div class="col-md-3 col-sm-3 col-xs-12 form-check">
						        <input class="form-check-input" type="radio" name="payment" value="wechat">
							  	<label class="form-check-label">
							    	Wechat
							  	</label>
				    		</div>
				    		<div class="col-md-3 col-sm-3 col-xs-12 form-check">
						        <input class="form-check-input" type="radio" name="payment" value="alipay">
							  	<label class="form-check-label">
							    	Alipay
							  	</label>
				    		</div>
				    		<div class="col-md-3 col-sm-3 col-xs-12 form-check">
				    			<input class="form-check-input" type="radio" name="payment" value="unionpay">
							  	<label class="form-check-label">
							    	Unionpay
							  	</label>
				    		</div>
				    	
				    	</div>
				  

				  <div class="form-group">
				    <div class="col-lg-8">
				      <center><button type="submit" class="btn btn-info">Next</button></center>
				    </div>
				  </div>

				</form>
			</div>
		<div class="col-md-2">
			aa
		</div>
	</div>
</div>

@extends('includes.footer', ['prev' => 'destination_address','next' => 'cc_info'])
@endsection