@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2><center>Total shipping cost is the below.<br>Select your payment way.</center></h2>

			<div class="row" style="margin-top: 50px;">
				<div class="col-sm-6">
					<h3>
						<center>Total shipping cost</center>
					</h3>
				</div><!-- .col-sm-6 -->
				<div class="col-sm-6">
					<h3>
						<center><?php echo number_format(Session::get('cost')).'円'; ?></center>
					</h3>
				</div><!-- .col-sm-6 -->
			</div><!-- .row -->
			
			<form id="form" class="form-horizontal" action={{ url('/dom_getpaymentmethod') }} method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			  	<div class="form-group">
			  		<div class="row">
			  			<label for="payment-method" class="control-label">Payment method</label>
			  		</div><!-- .row -->
			  		<div class="row" style="margin-top: 25px;">
			    		<div class="col-md-3 col-sm-3 col-xs-12 form-check">  
					        <input class="form-check-input payment" type="radio" name="payment" value="Credit Card" checked>
						  	<label class="form-check-label">
						    	Credit Card
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->
			    		<!--<div class="col-md-3 col-sm-3 col-xs-12 form-check">
					        <input class="form-check-input payment" type="radio" name="payment" value="Wechat">
						  	<label class="form-check-label">
						    	Wechat
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->
			    		<!--<div class="col-md-3 col-sm-3 col-xs-12 form-check">
					        <input class="form-check-input payment" type="radio" name="payment" value="Alipay">
						  	<label class="form-check-label">
						    	Alipay
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->
			    		<!--<div class="col-md-3 col-sm-3 col-xs-12 form-check">
			    			<input class="form-check-input payment" type="radio" name="payment" value="Unionpay">
						  	<label class="form-check-label">
						    	Unionpay
						  	</label>
			    		</div><!-- .col-md-3 col-sm-3 col-xs-12 form-check -->	
			    		<input type="hidden" id="stripeToken" name="stripeToken" value="">
			    	</div><!-- .row -->
			    </div><!-- .form-group -->
			</form>
		</div><!-- .col-md-8 -->

		@include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container-fluid -->
<script src="js/custom.js"></script>
@include('includes.footer', ['prev' => 'international_destination_address','next' => 'creditcard_payment', 'value' => 'step6'])
@endsection