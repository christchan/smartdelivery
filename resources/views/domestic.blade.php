@extends('layouts.vue_template')

@section('content')
<domestic :shop_id="{{ json_encode($shop_id) }}" :shop_name="{{ json_encode($shop_name) }}" :pricetable="{{ json_encode($price_table) }}" :contentitems="{{ json_encode($content_items) }}" :url="'{{ url('/') }}'" :locale="'{{ app()->getlocale() }}'" :lang="{{ json_encode(__('lang')) }}" :prefectures="{{ json_encode($prefectures) }}" :airports="{{ json_encode($airports) }}" :today="'{{ $today }}'" :stripe="{{ json_encode($stripe) }}" :user="{{ json_encode($user) }}"></domestic>

@endsection