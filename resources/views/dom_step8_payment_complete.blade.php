@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h2><center>Payment is completed.<br>Print and stick the below slip on your parcel. You can receive the same info in your email.</center></h2>
				<div class="col-md-8">
					<div class="row">
					<ul style="list-style-type:none">
						<li>SHIPPING CODE: </li>
						<li>ORDER DATE: <?php echo date('Y-m-d') ?></li>
						<li>DESTINATION ADD: </li>
						<li>DESTINATION TEL: </li>
						<li>SHOP NAME: </li>
						<li>CUSTOMER NAME: {{ Session::get('name') }}</li>
						<li>CUSTOMER TEL: {{ Session::get('phone') }}</li>
						<li>PARCEL SIZE: {{ Session::get('size') }}</li>
						<li>PARCEL WEIGHT: {{ Session::get('weight') }}</li>
						<li>PACKING: {{ Session::get('grandtotal') }}</li>
						<li>CONTENTS: </li>
						<li>TOTAL SHIPPING COST: ¥{{ Session::get('grandtotal') }}</li>
					</ul>
					</div><!-- .col-md-8 -->
				</div><!-- . -->
				<form class="form-horizontal">
					  <div class="form-group">
					    <div class="col-md-12">
					      <button type="button" id="print" class="btn btn-info center-block">Print</button>
					    </div><!-- .col-md-12 -->
					  </div><!-- .form-group -->
				</form>
			</div><!-- .col-md-8 -->
	</div><!-- .row -->
</div><!-- .container-fluid -->

<script src="js/custom.js"></script>
<script type="text/javascript">
	$("#print").click(function(){
		window.print();
	});
</script>

@include('includes.footer', ['prev' => 'shipping','next' => '', 'value' => 'step8'])
@endsection