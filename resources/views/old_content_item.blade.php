@extends('layouts.maintemplate', ['filename' => 'content_item'])

@section('content')

<div class="container-fluid">
	<div class="row">
		
			<!--<div class="col-md-2"></div>-->
				<div class="col-md-10">
				<div class="row">
					<div class="col-md-12">
          <div class="row">
						<h2><center>Enter the content items, unit price, and amount.</center></h2>
						<!--
						<form class="form-horizontal" action={{ url('/get_total') }} method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="col-md-3" style="margin-left:10px;">
                <div class="form-group-inline"><label for="content_items" class="col-md-12">Tes</label></div>
                <div class="form-group-inline"><label for="content_items" class="col-md-12">Tes</label></div>
              </div>

              <div class="col-md-3" style="margin-left:10px;">
                <div class="row">
                  <div class="form-group"><label for="content_items" class="col-md-12">Content Items</label></div>
                  <div class="col-md-12" id="1">
                  @for($i=0;$i<7;$i++)
                      <div class="form-group itemx">
                        <select class="form-control" id="content_items" name="content_items[]">
                          @foreach($contents as $content)
                          <option>{{ $content->name }}</option>
                          @endforeach
                        </select>
                      </div>
                  @endfor
                  </div>
                </div>
              </div>
              

              <div class="col-md-2" style="margin-left:10px;">
                <div class="row">
                  <div class="form-group"><label for="price" class="col-md-12">Unit price</label></div>
                  <div class="col-md-12" id="2">
                    @for($i=0;$i<7;$i++)
                    <div class="form-group input-group pricex">
                      <span class="input-group-addon">¥</span>
                      <input type="text" class="form-control" name="price[]" aria-label="Amount (to the nearest yen)" onchange="sum()">
                      <span class="input-group-addon">.00</span>
                    </div>
                  @endfor
                  </div>
                </div>
              </div>

              <div class="col-md-2" style="margin-left: 10px;">
                <div class="row">
                  <div class="form-group"><label for="piece" class="col-md-12">Pieces</label></div>
                  <div class="col-md-12" id="3">
                    @for($i=0;$i<7;$i++)
                    <div class="form-group piecex">
                        <select class="form-control" name="piece[]" onchange="sum()">
                          @for($i=1;$i<=99;$i++)
                          <option value={{ $i }}> {{ $i }}</option>
                          @endfor
                        </select>
                      </div>
                    @endfor
                  </div>
                  <label for="" class="col-md-12"><center>Grand Total Price</center></label>
                </div>
              </div>
              
              <div class="col-md-2" style="margin-left: 10px;">
                <div class="row">
                  <div class="form-group"><label for="" class="col-md-12">Total price</label></div>
                  <div class="col-md-12" id="4">
                    @for($i=0;$i<7;$i++)
                      <div class="form-group totalx"><input type="text" name="total[]" class="form-control total" readonly onchange="sum()"></div>
                    @endfor
                  </div>
                  <div class="form-group"><input type="text" id="grandtotal" name="grandtotal" class="form-control grandtotal" readonly></div>
                  </div>
                </div>
              
              
              <div class="col-md-1" style="margin-left: 10px;">
                <div class="row">
                <div class="form-group"><label>&nbsp;</label></div>
                  <div class="col-md-6" id="5">
                    @for($i=0;$i<7;$i++)
                    <div class="form-group"><button type="button" id="remove" name="remove" class="btn btn-danger form-control grandtotal" onclick="removerow()">-</button></div>
                    @endfor
                  </div>
                  <div class="form-group col-md-6"><button type="button" id="add" name="add" class="btn btn-primary form-control grandtotal" onclick="addrow()">+</button></div>
                </div>
              </div>

							
							 <div class="col-md-12">
						 		<center><button type="submit" class="btn btn-info btn-lg">Next</button></center>
							 </div>
						</form>
            -->
              <form class="form-inline" action={{ url('/get_total') }} method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-md-12" style="margin:10px;">
                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="exampleInputName2">Content Items</label>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail2">Unit Price</label>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail2">Pieces</label>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="exampleInputEmail2">Total price</label>
                  </div>
                </div>
                </div>
                @for($i=0;$i<6;$i++)
                <div class="col-md-12" style="margin:10px;">
                  <div class="form-group col-md-3">
                    <select class="form-control" id="content_items" name="content_items[]">
                      @foreach($contents as $content)
                      <option>{{ $content->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  
                  <div class="form-group col-md-3">
                    <!--<span class="input-group-addon">¥</span>-->
                    <input type="text" class="form-control" name="price[]" aria-label="Amount (to the nearest yen)" onchange="sum()">
                    <!--<span class="input-group-addon">.00</span>-->
                  </div>
                  
                  <div class="form-group col-md-1">
                    <select class="form-control" name="piece[]" onchange="sum()">
                      @for($j=1;$j<=99;$j++)
                      <option value={{ $j }}>{{ $j }}</option>
                      @endfor
                    </select>
                  </div>

                  <div class="form-group col-md-3">
                    <input type="text" name="total[]" class="form-control total" readonly onchange="sum()">
                  </div>
                  <div class="form-group col-md-2"><button type="button" id="remove" name="remove" class="btn btn-danger form-control remove">-</button></div>
                </div>
                @endfor
                <div id="field"></div>
                <div class="col-md-8"><div class="form-group"><input type="text" id="grandtotal" name="grandtotal" class="form-control grandtotal" readonly></div>
                <div class="form-group col-md-6"><button type="button" id="add" name="add" class="btn btn-primary form-control add">+</button></div>
                <div class="col-md-12">
                  <center><button type="submit" class="btn btn-info btn-lg">Next</button></center>
               </div>
              </form>
					</div>
					</div>
					</div>
			<!--<div class="col-md-2"></div>-->
		</div>

	</div>
  <div class="col-md-2">
      <div class="row">
          <div class="col-md-12">
          <div class="row">
            <labeL>Shipping Info</labeL>
            <br>Size : {{ Session::get('size') }}
            <br>Weight : {{ Session::get('weight') }}
            <br>Packed : 
            @if(Session::get('status')=="0")
              &#x2612;
            @elseif(Session::get('status')=="1")
              &#x2611;
            @else
              &nbsp;
            @endif
          </div>
          </div>
      </div>
  </div>
</div>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handsontable/0.35.1/handsontable.min.js"></script>
<link rel="stylesheet" type="text/css" src="https://cdnjs.cloudflare.com/ajax/libs/handsontable/0.35.1/handsontable.min.css" />-->

<!--
<link rel="stylesheet" type="text/css" href="https://docs.handsontable.com/pro/bower_components/handsontable-pro/dist/handsontable.full.min.css">
<!--<link rel="stylesheet" type="text/css" href="https://handsontable.com/static/css/main.css">-->
<!--<script src="https://docs.handsontable.com/pro/bower_components/handsontable-pro/dist/handsontable.full.min.js"></script>
-->

<!--
<script src="lib/RuleJS/lib/lodash/lodash.js"></script>
<script src="lib/RuleJS/lib/underscore.string/underscore.string.js"></script>
<script src="lib/RuleJS/lib/moment/moment.js"></script>
<script src="lib/RuleJS/lib/numeral/numeral.js"></script>
<script src="lib/RuleJS/lib/numericjs/numeric.js"></script>
<script src="lib/RuleJS/lib/js-md5/md5.js"></script>
<script src="lib/RuleJS/lib/jstat/jstat.js"></script>
<script src="lib/RuleJS/lib/formulajs/formula.js"></script>
-->
<!--
<script src="handsontable-ruleJS-master/bower_components/RuleJS/dist/js/parser.js"></script>
<script src="handsontable-ruleJS-master/bower_components/RuleJS/dist/js/ruleJS.js"></script>
-->
<!--
<script src="lib/handsontable/handsontable.formula.js"></script>
<link rel="stylesheet" media="screen" href="lib/handsontable/handsontable.formula.css">-->
<script>

$(document).ready(function(){
    var len = $('.total').length;

    var newitem = '<div class="form-group"><select class="form-control" id="content_items" name="content_items">@foreach($contents as $content)<option>{{ $content->name }}</option>@endforeach</select></div>';
    var newprice = '<div class="form-group input-group"><span class="input-group-addon">¥</span><input type="text" class="form-control" name="price" aria-label="Amount (to the nearest yen)" onchange="sum()"><span class="input-group-addon">.00</span></div>';
    var newpiece = '<div class="form-group"><select class="form-control" id="pieces" name="piece" onchange="sum()">@for($i=1;$i<=99;$i++)<option value={{ $i }}> {{ $i }}</option>@endfor</select></div>';
    var newtotal = '<div class="form-group"><input type="text" name="total" class="form-control total" onchange="sum()"></div>';
    var newremove = '<div class="form-group"><button type="button" id="remove" name="remove" class="btn btn-danger form-control grandtotal" onclick="removerow()">-</button></div>';
    
    var newfield = '<div class="col-md-12" style="margin:10px;"><div class="form-group col-md-3"><select class="form-control" id="content_items" name="content_items[]">@foreach($contents as $content)<option>{{ $content->name }}</option>@endforeach</select></div><div class="form-group col-md-3"><input type="text" class="form-control" name="price[]" aria-label="Amount (to the nearest yen)" onchange="sum()"></div><div class="form-group col-md-1"><select class="form-control" name="piece[]" onchange="sum()">@for($j=1;$j<=99;$j++)<option value={{ $j }}>{{ $j }}</option>@endfor</select></div><div class="form-group col-md-3"><input type="text" name="total[]" class="form-control total" readonly onchange="sum()"></div><div class="form-group col-md-2"><button type="button" id="remove" name="remove" class="btn btn-danger form-control grandtotal" onclick="removerow()">-</button></div></div>';

    $("#add").click(function(){
      /*
        $("#1").append(newitem);
        $("#2").append(newprice);
        $("#3").append(newpiece);
        $("#4").append(newtotal);
        $("#5").append(newremove);
      */
        $("#field").append(newfield);
    });
    /*
    $("#field").on("click", ".remove", (function(){

        //$("#field").append(newfield);
        //$("#1").remove(.itemx:nth-child(len));
        //$("#2").remove(.pricex:nth-child(len));
        //$("#3").remove(.piecex:nth-child(len));
        //$("#4").remove(.totalx:nth-child(len));
        $("#field").parent().remove();
        //console.log($("#field").parent());
      
    });
    */
    //source : http://jsfiddle.net/0uv4k5bz/2/
     /*
    $("#dftenglist").on("click", ".minusbtn", function() {
      $(this).parent().remove();
    });
    */
    
});

    function sum()
    {
        var grandtotal = 0;
        
        var prices = document.getElementsByName('price[]');
        var pieces = document.getElementsByName('piece[]');
        var totals = document.getElementsByName('total[]');

        for (var i=0;i<$('.total').length;i++) {
            var price = parseInt(prices[i].value) || 0;
            var piece = parseInt(pieces[i].value) || 0;
            
            totals[i].value = price * piece;
            grandtotal += parseInt(totals[i].value);
        }
        console.log(grandtotal);
        document.getElementById('grandtotal').value = grandtotal;
    }

/*
var cek = <?php //echo json_encode($cek); ?>;
var pieces = <?php //echo json_encode($pieces); ?>;
var dataObject = <?php echo json_encode($contents); ?>;

var generateDataObj = function(rows) {
    var data = [];
    var number = 0;
  
    if (!rows) {
      rows = 3;
    }
  
    for (var i = 0; i < rows; i++) {
      data[i] = [];
      for (var j = 0; j < 5; j++) {
        data[i][j] = number;
      }
    }
  
    for (i = 0; i < 2; i++) {
      data.push([]);
    }
  
    return data;
  };

var hotElement = document.querySelector('#hot');
var hotElementContainer = hotElement.parentNode;
var hotSettings = {
  data: generateDataObj(),
  columns: [
    {
        type: 'dropdown',
        source: cek,
    },
    {
      type: 'numeric',
      numericFormat: {
        pattern: '¥0,0.00',
        //culture: 'ja-JP'
      }
    },
    {
      type: 'dropdown',
      source: pieces
    },
    {
      data: ['10'],
      type: 'numeric',
      numericFormat: {
        pattern: '¥0,0.00',
      }
    }
    /*
    
    {
      data: 'level',
      type: 'numeric',
      numericFormat: {
        pattern: '0.0000'
      }
    },
    {
      data: 'units',
      type: 'text'
    },
    {
      data: 'asOf',
      type: 'date',
      dateFormat: 'MM/DD/YYYY'
    },
    {
      data: 'onedChng',
      type: 'numeric',
      numericFormat: {
        pattern: '0.00%'
      }
    }
    
  ],
  columnSummary: [
      {
        sourceRow: 0,
        destinationRow: 0,
        destinationColumn: 3,
        reversedRowCoords: true,
        type: 'sum',
        forceNumeric: true
      }
  ],
  stretchH: 'all',
  width: 806,
  autoWrapRow: true,
  height: 487,
  maxRows: 30,
  manualRowResize: true,
  manualColumnResize: true,
  rowHeaders: true,
  colHeaders: [
    'Content Items',
    'Unit Price',
    'Pieces',
    'Total Price'
  ],
  manualRowMove: true,
  manualColumnMove: true,
  contextMenu: true,
  filters: true,
  dropdownMenu: false
};

var hot = new Handsontable(hotElement, hotSettings);
*/
</script>

@extends('includes.footer', ['prev' => 'size_dom','next' => 'destination_address'])
@endsection