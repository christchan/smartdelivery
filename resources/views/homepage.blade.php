@extends('layouts.vue_template')

@section('content')
<div id="home">
	<chooseshipping :lang="{{ json_encode(__('lang')) }}" :url="'{{ url('/') }}'" :shop_id="'{{ $shop_id }}'" :shop_name="{{ json_encode($shop_name) }}"></chooseshipping>
</div>
@endsection