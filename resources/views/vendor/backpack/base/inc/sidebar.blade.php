@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/domestic_shipping') }}"><i class="fa fa-square"></i> <span>Domestic Shipping</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/overseas_shipping') }}"><i class="fa fa-square"></i> <span>Overseas Shipping</span></a></li>
          <!-- <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/airport') }}"><i class="fa fa-square"></i> <span>Airports</span></a></li> -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/contentitem') }}"><i class="fa fa-square"></i> <span>Content Items</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/country') }}"><i class="fa fa-square"></i> <span>Country</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/language') }}"><i class="fa fa-square"></i> <span>Languages</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/page') }}"><i class="fa fa-square"></i> <span>Pages</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/prefecture') }}"><i class="fa fa-square"></i> <span>Prefectures</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/prefecture_shipping_area') }}"><i class="fa fa-square"></i> <span>Prefecture Shipping Areas</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/shop') }}"><i class="fa fa-square"></i> <span>Shops</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/shipping_area') }}"><i class="fa fa-square"></i> <span>Shipping Areas</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/shipping_cost') }}"><i class="fa fa-square"></i> <span>Shipping Cost</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/international_shipping_cost') }}"><i class="fa fa-square"></i> <span>International Shipping Cost</span></a></li>

          <!-- Users, Roles Permissions -->
          <li class="treeview">
            <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
            </ul>
          </li>


          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
