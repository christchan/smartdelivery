<?php
$startDate = '';
$endDate = '';
if (isset($_GET['from']) && isset($_GET['to'])) {
	$startDate = $_GET['from'];
	$endDate = $_GET['to'];
}
?>
<div class="daterange-filter pull-right">
	<div class="form-group m-b-0">
		<div class="input-group date">
	        <div class="input-group-addon">
	          <i class="fa fa-calendar"></i>
	        </div>
	        <input class="form-control pull-right daterangepicker" type="text">
	        <div class="input-group-addon">
	          <a class="daterangepicker-clear-button" href=""><i class="fa fa-times"></i></a>
	        </div>
	    </div>
	</div>
	<button class="btn btn-primary downloadcsv">
		<?php echo __('lang.download_csv'); ?>
	</button>
</div>

{{-- ########################################### --}}
{{-- Extra CSS and JS for this particular filter --}}

{{-- FILTERS EXTRA CSS  --}}
{{-- push things in the after_styles section --}}

@push('crud_list_styles')
    <!-- include select2 css-->
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	<style>
		.input-group.date {
			width: 320px;
			max-width: 100%;
		}
		.daterange-filter{

		}
		.daterangepicker{
			top: 0;
			left: 0;
		}
		.daterangepicker.dropdown-menu {
			z-index: 3001!important;
		}
		.form-group{
			display: inline-block;
		}
		.downloadcsv{
			display: inline-block;
			vertical-align: top;
			margin-left: 10px;
		}
	</style>
@endpush


{{-- FILTERS EXTRA JS --}}
{{-- push things in the after_scripts section --}}

@push('crud_list_scripts')
	<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  	<script>

  		function applyDateRangeFilter(start, end) {
  			if (start && end) {
  				var dates = {
					'from': start.format('YYYY-MM-DD'),
					'to': end.format('YYYY-MM-DD')
				};
				var value = JSON.stringify(dates);
  			} else {
  				//this change to empty string,because addOrUpdateUriParameter method just judgment string
  				var value = '';
  			}
  		}

		jQuery(document).ready(function($) {
			var dateRangeInput = $('.daterangepicker').daterangepicker({
				timePicker: false,
		        ranges: {
		            'Today': [moment().startOf('day'), moment().endOf('day')],
		            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		            'This Month': [moment().startOf('month'), moment().endOf('month')],
		            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        },
		        @if ($startDate != '' && $endDate != '')
		        startDate: moment("{{ $startDate }}"),
		        endDate: moment("{{ $endDate }}"),
				autoUpdateInput: true,
				@else
				autoUpdateInput: false,
				@endif
				autoApply: true,
				alwaysShowCalendars: true,
			},
			function (start, end) {
				var startDate = start.format('YYYY-MM-DD');
				var endDate = end.format('YYYY-MM-DD');
				applyDateRangeFilter(start, end);
				window.location = window.location.origin + window.location.pathname + "?from="+startDate+"&to="+endDate;
			});

			$('.daterangepicker-clear-button').click(function(e){
				e.preventDefault();
				window.location = window.location.origin + window.location.pathname;
				console.log(window.location)
			});

			$('.downloadcsv').click(function(){
				$.ajax({
			        url: "{{ url('/') }}" + "/downloadcsv/international",
			        method: 'POST',
			        data: {from: "{{ $startDate }}", to: "{{ $endDate }}"},
			        dataType: 'json',
			        success: function(data) {
			            if (data.status == "success") {
			            	window.open(window.location.origin+"/csv/"+data.data);
			            }
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            console.log(jqXHR);
			        }
			    });
			});


		});
  	</script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}