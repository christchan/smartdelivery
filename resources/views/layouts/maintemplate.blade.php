<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?php
        $encrypter          = app('Illuminate\Encryption\Encrypter');
        $encrypted_token    = $encrypter->encrypt(csrf_token());
    ?>
    <!-- CSRF Token -->
    <meta name="_token" content="{{ $encrypted_token }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- CSS And JavaScript -->
    @include('includes.css')
    @include('includes.javascript')

    <link href="{{ URL::asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/'.$filename.'.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('js/'.$filename.'.js') }}" type="text/javascript"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script>
    var url = "{{ URL::to('/') }}";
    </script>

</head>
<style>
h1{
    color:#28544B;
    font-size:40px;
}

#chartdiv {
  max-width: 1100px;
  height: 400px;
  border: 1px solid #ddd;
  margin: 0 auto;
}

#info {
  max-width: 1100px;
  border: 1px solid #ddd;
  margin: 10px auto;
  padding: 5px 8px;
}

.btn-big{
    color:white;
    background:#28544B;
    line-height:40px;
    height:200px;
    display: block;
    font-size:30px;
}

#footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: white;
   color: white;
   border-top: 1px solid rgb(151,151,151);
   z-index: 2;
}
#footer button{
    background-color:#28544B;
    width: 100%;
    color: white;
    font-size: 18px;
    border-left: 1px solid rgb(151,151,151);
    border-right: 1px solid rgb(151,151,151);
    border-radius: 0;
}
</style>
<body>
    
    <input id="token" type="hidden" value="{{$encrypted_token}}">
    @include('includes.header')
    
    {{-- <div class="container-fluid"> --}}
        {{-- <div class="row"> --}}
            
            
                @yield('content')
            
        
        {{-- </div> --}}
    {{-- </div> --}}

    {{-- @include('includes.footer') --}}
<div style="display:block;height:60px;"></div>

</body>
</html>
