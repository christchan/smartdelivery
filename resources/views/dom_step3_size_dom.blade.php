@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')
<div class="container">
	<div class="row">
		@include('includes.progress-step', ['current' => '2'])
			<div class="col-sm-8 col-sm-offset-2">
				<div class="row">
					<h2 class="text-center">Enter the total size and weight for domestic shipping.</h2>
					<br>
					<form id="form" class="form-horizontal" action={{ url('/get_size_dom') }} method="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
				  		<label for="packing" class="col-sm-4 control-label">Packing &amp; cushioning status</label>
				    	<div class="col-sm-8">
				    		<div class="row">
								<div class="form-group selectdelivery">
									<div class="radio col-md-3 col-sm-6 col-xs-6">
										<label class="" style="min-height: 78px;">
											<input type="radio" name="deliveryselection" class="deliveryselection radio col-md-3 col-sm-6 col-xs-6" value="1" checked="" data-switchon="0">Completed
										</label>
									</div><!-- .radio col-md-3 col-sm-6 col-xs-6 -->
									<div class="radio col-md-3 col-sm-6 col-xs-6">
										<label style="min-height: 78px;" class="checked">
											<input type="radio" name="deliveryselection" class="deliveryselection radio col-md-3 col-sm-6 col-xs-6" value="0" data-switchon="1">
											Not yet (Order it)
											<div style="color:blue;">+packing cost 500 JPY
											+packing material weight 1.0kg</div>
										</label>
									</div><!-- .radio col-md-3 col-sm-6 col-xs-6 -->
								</div><!-- .form-group selectdelivery -->
				  			</div><!-- .row -->
				  		</div><!-- .col-sm-8 -->
				  	</div><!-- .form-group -->
				  <div class="form-group">
				    <label for="Size" class="col-sm-4 control-label">Size (H+W+L = total cm)</label>
				    <div class="col-sm-6">
				      <input type="text" id="size" name="size" class="form-control" required>
				    </div><!-- .col-sm-6 -->
				  </div><!-- .form-group -->
				  <div class="form-group">
				    <label for="Weight" class="col-sm-4 control-label">Weight (kg)</label>
				    <div class="col-sm-6">
				      <input type="text" id="weight" name="weight" class="form-control" required>
				    </div><!-- .col-sm-6 -->
				  </div><!-- .form-group -->
					</form>
				</div><!-- .row -->
			</div><!-- .col-sm-8 col-sm-offset-2 -->
	</div><!-- .row -->
</div><!-- .container -->

<script src="js/custom.js"></script>

@include('includes.footer', ['prev' => 'domestic_area','next' => 'domestic_content_item', 'value' => 'step3'])
@endsection