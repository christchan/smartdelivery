@extends('layouts.maintemplate', ['filename' => 'domestic'])

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			@include('includes.progress-step', ['current' => '1'])
		</div>
			<div class="col-md-8 col-md-offset-2">
				<div class="image-content" style="padding:20px;border:1px solid black">
					<div class="row">
						<div class="col-md-12">
							<img src="{{ asset('images/domesticrules.png') }}" alt="" class="img img-responsive img-rounded">
						</div><!-- .col-md-12 -->
					</div><!-- .row -->
				</div><!-- .image-content -->
			</div><!-- .col-md-8 -->
	</div><!-- .row -->
</div><!-- .container-fluid -->

<script src="js/custom.js"></script>

@include('includes.footer', ['prev' => '/','next' => 'domestic_area', 'value' => 'dom_step1'])
@endsection