@extends('layouts.maintemplate', ['filename' => 'ems'])

@section('content')
<div class="container-fluid">
	<div class="row">
		<div id="domestic">
			<div class="">
				<div class="col-md-8 col-md-offset-2">
					<div class="row">
						<div class="col-md-12" style="">
							<div class="image-content" style="padding:20px;border:1px solid black">
								<div class="row">
									<div class="col-md-12">
										<div id="chartdiv"></div>
										<div id="info">Selected countries: <span id="selected">-</span></div>
									</div>
									<div class="col-md-12">
										<img src="{{ asset('images/overseasbot.png') }}" alt="" class="img img-responsive img-rounded">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
<script src="https://www.amcharts.com/lib/3/ammap.js"></script>
<script src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<script>
/**
 * Create the map
 */
var map = AmCharts.makeChart("chartdiv", {
  "type": "map",
  "theme": "light",
  "projection": "eckert3",
  "dataProvider": {
    "map": "worldLow",
    "getAreasFromMap": true
  },
  "areasSettings": {
    "selectedColor": "#CC0000",
    "selectable": true
  },
  /**
   * Add click event to track country selection/unselection
   */
  "listeners": [{
    "event": "clickMapObject",
    "method": function(e) {
      
      // Ignore any click not on area
      if (e.mapObject.objectType !== "MapArea")
        return;
      
      var area = e.mapObject;
      
      // Toggle showAsSelected
      area.showAsSelected = !area.showAsSelected;
      e.chart.returnInitialColor(area);
      
      // Update the list
      document.getElementById("selected").innerHTML = JSON.stringify(getSelectedCountries());
    }
  }]
});

/**
 * Function which extracts currently selected country list.
 * Returns array consisting of country ISO2 codes
 */
function getSelectedCountries() {
  var selected = [];
  for(var i = 0; i < map.dataProvider.areas.length; i++) {
    if(map.dataProvider.areas[i].showAsSelected)
      selected.push(map.dataProvider.areas[i].id);
  }
  return selected;
}
</script>

<script src="js/custom.js"></script>
@include('includes.footer', ['prev' => 'international_rule','next' => 'size_int', 'value' => 'int_step2'])
@endsection