@extends('layouts.vue_template')

@section('content')
<div id="myshipments">
	<myshipments :lang="{{ json_encode(__('lang')) }}"></myshipments>
</div>
@endsection