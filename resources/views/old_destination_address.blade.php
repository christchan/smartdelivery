@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')

<div class="container-fluid">
	<div class="row">
		<div id="loginpage">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="row">
						<h2><center>Enter the domestic destination address.</center></h2>
						<br>
						<form class="form-horizontal" action={{ url('/get_address_data') }} method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						  <div class="form-group-inline">
				    		<div class="form-group selectdelivery">
								<div class="radio-inline col-md-3 col-sm-6 col-xs-6">
									<label class="" style="min-height: 78px;">
									<input type="radio" name="deliveryselection" class="deliveryselection" value="0" data-switchon="0">
									Domestic Address
									</label>
								</div>
								<div class="radio-inline col-md-3 col-sm-6 col-xs-6">
									<label style="min-height: 78px;">
										<input type="radio" name="deliveryselection" class="deliveryselection" value="1" checked="checked" data-switchon="1">
										Domestic Airport
									</label>
								</div>
							</div>
						  </div>
							
						<div class="domestic-address-form" style="display: none;">
							  <div class="form-group">
							    <label for="" class="col-sm-4 control-label">Destination Prefecture</label>
							    <div class="col-sm-8">
							      <select class="form-control" name="prefecture" id="airport">
							      	@foreach($prefectures as $prefecture)
								    	<option>{{ $prefecture->name }}</option>
								    @endforeach
								  </select>
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="" class="col-sm-4 control-label">Destination Address</label>
							    <div class="col-sm-8">
							      <input type="text" name="address" class="form-control">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="" class="col-sm-4 control-label">Destination post code</label>
							    <div class="col-sm-8">
							      <input type="text" name="post-code" class="form-control">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="" class="col-sm-4 control-label">Customer Tel</label>
							    <div class="col-sm-8">
							      <input type="text" name="phone" class="form-control">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="" class="col-sm-4 control-label">Customer Name</label>
							    <div class="col-sm-8">
							      <input type="text" name="name" class="form-control">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="" class="col-sm-4 control-label">Email</label>
							    <div class="col-sm-8">
							      <input type="text" name="email" class="form-control">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="" class="col-sm-4 control-label">Delivery date</label>
							    <div class="col-sm-8">
							      <input type="text" name="delivery-date" class="form-control" readonly>
							    </div>
							  </div>
							  </div>
						  </div>

						  <div class="domestic-airport-form" style="display: none;">
						  	<div class="col-md-6">
						  		<div id="map"></div>
						  	</div>
						    <div class="form-group col-md-6">
						    	@foreach($airports as $airport)
						      	<button type="button" name="ap" value="{{ $airport->id }}" class="btn btn-lg btn-primary btn-block ap">{{ $airport->name }}</button>
						      	@endforeach
						    </div>
						  	<div class="col-md-12" style="margin-top: 30px;">
						  		<div class="col-md-12">
						  			<div class="col-sm-8">
						  				<label class="col-sm-4">Airport Information</label>
						  			</div>
								    <div class="col-sm-8">
								      <label id="airport-name">Name</label>
								    </div>
								  	<div class="col-sm-8">
								      <label id="airport-address">Address</label>
								    </div>
								    <div class="col-sm-8">
								      <label id="airport-tel">Telephone</label>
								    </div>
								    <div class="col-sm-8">
								      <label id="airport-code">Code</label>
								    </div>
						  		</div>
						  		<div class="col-md-12">
								  <div class="form-group">
								    <label for="" class="col-sm-4 control-label">Flight No.</label>
								    <div class="col-sm-8">
								      <input type="text" name="flight-no" class="form-control">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="" class="col-sm-4 control-label">Departure date</label>
								    <div class="col-sm-8">
								      <input type="datetime-local" name="departure-date" class="form-control">
								    </div>
								  </div>
								  <!--
								  <div class="form-group">
								    <label for="" class="col-sm-4 control-label">Time</label>
								    <div class="col-sm-8">
								      <input type="text" name="time" class="form-control">
								    </div>
								  </div>
								  -->
							  	</div>
						  	</div>
						  </div>
						  	<div class="form-group col-md-12">
						      <center><button type="submit" class="btn btn-lg btn-info">Next</button></center>
						    </div>
						</form>
				</div>
			</div>
			<div class="col-md-2"></div>
			
		</div>
	</div>
	<div class="col-md-2">
      <div class="row">
          <div class="col-md-12">
          <div class="row">
            <labeL>Shipping Info</labeL>
            <br>Size : {{ Session::get('size') }}
            <br>Weight : {{ Session::get('weight') }}
            <br>Packed : 
            @if(Session::get('status')=="0")
              &#x2612;
            @else
              &#x2611;
            @endif
          </div>
          </div>
      </div>
  </div>
</div>

<style>
#map {
  height: 400px;
  width: 100%;
 }

.btn-primary:active:focus {
  color: red; 
  background-color: #161617; 
  border-color: #494F57;
}
</style>

<script src="js/custom.js"></script>
<script>

	var airports = <?php echo $airports ?>;

  function initMap() {
  	
    var coordinate = {lat: 35.4875472, lng: 137.5602275};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 6,
      center: coordinate
    });

    var image = {
	    url: "images/airport.png",
	    // This marker is 20 pixels wide by 32 pixels high.
	    scaledSize: new google.maps.Size(40, 40),
	    origin: new google.maps.Point(0,0), // origin
    	anchor: new google.maps.Point(0, 0) // anchor
  	};
    
  	var infowindow = new google.maps.InfoWindow();

    for(var i=0;i<airports.length;i++)
    {
    	var airport_coordinate = {lat: airports[i].latitude, lng: airports[i].longitude};
	    var marker = new google.maps.Marker({
	      position: airport_coordinate,
	      title: airports[i].name,
	      map: map,
	      icon: image
	    });

	    google.maps.event.addListener(marker, 'click', (function(marker, i) {
	    	return function() {
		    	updateAirportInfo(airports[i].name, airports[i].address, airports[i].tel, airports[i].postcode);
		    }
		})(marker, i));
	}	
  }
  
  function updateAirportInfo(name, address, tel, code)
  {
  		var airport_name = document.getElementById('airport-name');
  		var airport_address = document.getElementById('airport-address');
  		var airport_tel = document.getElementById('airport-tel');
  		var airport_code = document.getElementById('airport-code');
  		airport_name.innerHTML = name;
  		airport_address.innerHTML = address;
  		airport_tel.innerHTML = tel;
  		airport_code.innerHTML = code;
  }
  
/*
  	$(".ap").click(function() {
	  	$( "ap" ).each(function( i ) {
		    if ( this.style.color !== "blue" ) {
		      this.style.color = "blue";
		    } else {
		      this.style.color = "";
		    }
  		});

	});
	*/
	/*
  $(".ap").click(function(e) {
  		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

	    e.preventDefault();
	    $.ajax({
	        type: "POST",
	        url: "{{ url('/getAirportCoordinate') }}",
	        data: { 
	            id: $(this).val()
	        },
	        success: function(data) {
	            var coordinate = {lat: data[0].latitude, lng: data[0].longitude};
	            var map = new google.maps.Map(document.getElementById('map'), {
			      zoom: 9,
			      center: coordinate
			    });
	            var marker = new google.maps.Marker({
			      position: coordinate,
			      map: map
			    });
			    updateAirportInfo(data[0].name, data[0].address, data[0].tel, data[0].postcode);
	        },
	        error: function(ts) {
	            //console.log('Error:', data);
	            console.log(ts.responseText);
	        }
	    });
	});
	*/
	$(".ap").click(function(e) {
  		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

	    e.preventDefault();
	    $.ajax({
	        type: "POST",
	        url: "{{ url('/getAirportCoordinate') }}",
	        data: { 
	            id: $(this).val()
	        },
	        success: function(data) {
			    updateAirportInfo(data[0].name, data[0].address, data[0].tel, data[0].postcode);
	        },
	        error: function(ts) {
	            //console.log('Error:', data);
	            console.log(ts.responseText);
	        }
	    });
	});
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWxgd1LEqFR9sYXYDuJlE5IG8X6jy0DSY&callback=initMap"></script>

@extends('includes.footer', ['prev' => 'content_item','next' => 'shipping'])
@endsection