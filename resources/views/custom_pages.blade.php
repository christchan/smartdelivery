@extends('layouts.vue_template')

@section('content')
<div id="custom_page">
<v-layout>
    <v-flex xs12>
      <v-card flat>
        <v-card-title primary-title>
          <div>
            <h3 class="headline mb-0">{!! $page->title !!}</h3>
            <div>{!! $page->content !!}</div>
          </div>
        </v-card-title>
      </v-card>
    </v-flex>
  </v-layout>
</div>
@endsection