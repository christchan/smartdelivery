@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2><center>Scan the below code with your Alipay Payment App.</center></h2>
			<form class="form-horizontal">
				<center>JPY: {{ Session::get('grandtotal') }}</center>
			    <div class="form-group">
			    	<button type="button" class="btn btn-info center-block">Next</button>
			  	</div><!-- .form-group -->
			</form>
		</div><!-- .col-md-8 -->
		@include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container-fluid -->

@extends('includes.footer', ['prev' => 'shipping','next' => 'payment_complete'])
@endsection