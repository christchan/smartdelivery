<style>
	table {

	}

	td {
		font-size: 14px;
	}
</style>

<table>
	<tr>
		<td>{{ trans('lang.shipping_code') }}</td>
		<td>{{ $order->shipping_code }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.order_date') }}</td>
		<td>{{ $order->order_date }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.destination_address') }}</td>
		<td>{{ $order->destination_address }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.destination_tel') }}</td>
		<td>{{ $order->destination_tel }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.shop_name') }}</td>
		<td>{{ $order->shop_name }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.customer_name') }}</td>
		<td>{{ $order->customer_name }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.customer_tel') }}</td>
		<td>{{ $order->customer_tel }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.parcel_size') }}</td>
		<td>{{ $order->parcel_size }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.parcel_weight') }}</td>
		<td>{{ $order->parcel_weight }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.packing') }}</td>
		<td>{{ $order->packing_status }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.contents') }}</td>
		<td>{{ $order->contents }}</td>
	</tr>
	<tr>
		<td>{{ trans('lang.total_shipping_cost') }}</td>
		<td>{{ $order->total_shipping_cost }}</td>
	</tr>
</table>