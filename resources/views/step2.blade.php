@extends('layouts.maintemplate', ['filename' => 'step2'])

@section('content')
<div id="step2-page">
		<div class="col-md-4 col-md-offset-4">
			<div class="row">
				<div class="col-md-12">
					<a href="{{ url('/domestic_rule') }}" class="">
						<button class="btn btn-option btn-big" style="width:100%;margin:10px 0;">{{ trans('lang.home_dom') }}</button>
					</a>
				</div>
				<div class="col-md-12">
					<a href="{{ url('/international_rule') }}" class="">
						<button class="btn btn-option btn-big" style="width:100%;margin:10px 0;">{{ trans('lang.home_int') }}</button>
					</a>
				</div>
			</div>
		</div>
</div>
@endsection