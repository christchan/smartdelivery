@extends('layouts.maintemplate', ['filename' => 'size'])

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2><center>Scan the below code with your Unionpay Payment App.</center></h2>
			<form class="form-horizontal">
				<div class="col-md-12">
					<center>JPY: {{ Session::get('grandtotal') }}</center>
				</div><!-- .col-md-12 -->
			    <div class="form-group">
			    	<div class="col-md-12">
			      		<center><button type="button" class="btn btn-info">Next</button></center>
			    	</div><!-- .col-md-12 -->
			  	</div><!-- .form-group -->
			</form>
		</div><!-- .col-md-8 -->
		@include('includes.infosidebar')
	</div><!-- .row -->
</div><!-- .container-fluid -->

@extends('includes.footer', ['prev' => 'shipping','next' => 'payment_complete'])
@endsection