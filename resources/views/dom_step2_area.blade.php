@extends('layouts.maintemplate', ['filename' => 'area'])

@section('content')
<div class="container">
	<div class="row">	
	@include('includes.progress-step', ['current' => '1'])
		<div class="col-md-12">
			<h2 class="text-center">{{ trans('lang.domestic_rule_title') }}</h2>
			<div class="row">
				<div class="col-md-4">
					<div class="image-content" style="padding:20px;border:1px solid black">
						<div class="row">	
							<img src="{{ asset('images/domesticrate.png') }}" alt="" class="img img-responsive img-rounded">	
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="table-responsive">
						<table class="table table-bordered table-hover" border="1">
							<thead>
							<tr>
								<th colspan="8" style="background-color:#ff98a5;"><center>Domestic shipping cost</center></th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td></td>
								<td></td>
								<td class="text-center" colspan="2">Other than the right area</td>
								<td class="text-center" colspan="2">Okayama/hiroshima/Yamaguchi/<br>Tottori/Shimane/Kagawa/Tokushima/Kouchi/Ehime</td>
								<td class="text-center" colspan="2">DOMESTIC AIRPORT</td>
							</tr>
							<tr>
								<td class="text-center">Weight(kg)</td>
								<td class="text-center">Total size(cm)</td>
								<td class="text-center">Packed</td>
								<td class="text-center">Non-packed</td>
								<td class="text-center">Packed</td>
								<td class="text-center">Non-packed</td>
								<td class="text-center">Packed</td>
								<td class="text-center">Non-packed</td>
							</tr>
							@foreach($price_table as $weight => $totalsize)
							<tr>
								<td>≤{{ $weight }}</td>
								@foreach($totalsize as $size => $value)
								<td>≤{{ $size }}</td>
									@foreach($value as $price)
									@if($price->packing_status == 1)
									<td style="background-color: #ff98a5;">¥{{ number_format($price->price) }}</td>
									@else
									<td>¥{{ number_format($price->price) }}</td>
									@endif
									@endforeach
								@endforeach
							</tr>
							@endforeach
							<!-- <tr>
								<td>≤2</td>
								<td>≤60</td>
								<td style="background-color: #ff98a5;">¥864</td>
								<td>¥1,188</td>
								<td style="background-color: #ff98a5;">¥1,188</td>
								<td>¥1,512</td>
								<td style="background-color: #ff98a5;">¥1,512</td>
								<td>¥1,836</td>
							</tr>
							<tr>
								<td>≤5</td>
								<td>≤80</td>
								<td style="background-color: #ff98a5;">¥972</td>
								<td>¥1,296</td>
								<td style="background-color: #ff98a5;">¥1,188</td>
								<td>¥1,512</td>
								<td style="background-color: #ff98a5;">¥1,620</td>
								<td>¥1,944</td>
							</tr>
							<tr>
								<td>≤10</td>
								<td>≤100</td>
								<td style="background-color: #ff98a5;">¥1,080</td>
								<td>¥1,404</td>
								<td style="background-color: #ff98a5;">¥1,296</td>
								<td>¥1,620</td>
								<td style="background-color: #ff98a5;">¥1,728</td>
								<td>¥2,052</td>
							</tr>
							<tr>
								<td>≤15</td>
								<td>≤120</td>
								<td style="background-color: #ff98a5;">¥1,080</td>
								<td>¥1,404</td>
								<td style="background-color: #ff98a5;">¥1,296</td>
								<td>¥1,620</td>
								<td style="background-color: #ff98a5;">¥1,728</td>
								<td>¥2,052</td>
							</tr>
							<tr>
								<td>≤20</td>
								<td>≤140</td>
								<td style="background-color: #ff98a5;">¥1,080</td>
								<td>¥1,404</td>
								<td style="background-color: #ff98a5;">¥1,296</td>
								<td>¥1,620</td>
								<td style="background-color: #ff98a5;">¥1,728</td>
								<td>¥2,052</td>
							</tr>
							<tr>
								<td>≤25</td>
								<td>≤160</td>
								<td style="background-color: #ff98a5;">¥1,080</td>
								<td>¥1,404</td>
								<td style="background-color: #ff98a5;">¥1,296</td>
								<td>¥1,620</td>
								<td style="background-color: #ff98a5;">¥1,728</td>
								<td>¥2,052</td>
							</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="js/custom.js"></script>
@include('includes.footer', ['prev' => 'domestic_rule','next' => 'size_dom', 'value' => 'dom_step2'])
@endsection