@extends('layouts.vue_template')

@section('content')
<profile :user="{{ json_encode($user) }}" :lang="{{ json_encode(__('lang')) }}" :url="'{{ url('/') }}'"></profile>
@endsection