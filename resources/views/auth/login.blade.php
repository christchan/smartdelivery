@extends('layouts.vue_template')

@section('content')
<login :url="'{{ url('/') }}'" :lang="{{ json_encode(__('lang')) }}"></login>
@endsection
