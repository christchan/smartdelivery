@extends('layouts.vue_template')

@section('content')
<register :url="'{{ url('/') }}'" :lang="{{ json_encode(__('lang')) }}"></register>
@endsection
