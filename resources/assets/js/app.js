
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vuetify from 'vuetify'; 
Vue.use(Vuetify)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('headermenu', require('./components/HeaderMenu.vue'));
Vue.component('domestic', require('./components/Domestic.vue'));
Vue.component('domestic-rules', require('./components/Domestic-Rules.vue'));
Vue.component('domestic-cost', require('./components/Domestic-Cost.vue'));
Vue.component('domestic-size', require('./components/Domestic-Size.vue'));
Vue.component('domestic-items', require('./components/Domestic-Items.vue'));
Vue.component('domestic-destination', require('./components/Domestic-Destination.vue'));
Vue.component('domestic-payment', require('./components/Domestic-Payment.vue'));
Vue.component('domestic-finish', require('./components/Domestic-Finish.vue'));
Vue.component('sidebar-shippinginformation', require('./components/Sidebar-ShippingInformation.vue'));
Vue.component('international', require('./components/International.vue'));
Vue.component('international-rules', require('./components/International-Rules.vue'));
Vue.component('international-ems', require('./components/International-Ems.vue'));
Vue.component('international-size', require('./components/International-Size.vue'));
Vue.component('international-items', require('./components/International-Items.vue'));
Vue.component('international-destination', require('./components/International-Destination.vue'));
Vue.component('international-payment', require('./components/International-Payment.vue'));
Vue.component('international-finish', require('./components/International-Finish.vue'));
Vue.component('login', require('./components/Login.vue'));
Vue.component('register', require('./components/Register.vue'));
Vue.component('selectshop', require('./components/SelectShop.vue'));
Vue.component('profile', require('./components/Profile.vue'));
Vue.component('chooseshipping', require('./components/ChooseShipping.vue'));
Vue.component('myshipments', require('./components/MyShipments.vue'));

const app = new Vue({
    el: '#app'
});
