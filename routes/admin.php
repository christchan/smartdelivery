<?php 

CRUD::resource('airport', 'AirportCrudController');
CRUD::resource('contentitem', 'ContentItemCrudController');
CRUD::resource('country', 'CountryCrudController');
CRUD::resource('language', 'LanguageCrudController');
CRUD::resource('page', 'PageCrudController');
CRUD::resource('prefecture', 'PrefectureCrudController');
CRUD::resource('prefecture_shipping_area', 'PrefectureShippingAreaCrudController');
CRUD::resource('shipping_area', 'ShippingAreaCrudController');
CRUD::resource('shipping_cost', 'ShippingCostCrudController');
CRUD::resource('international_shipping_cost', 'InternationalShippingCostCrudController');
CRUD::resource('shop', 'ShopCrudController');
CRUD::resource('domestic_shipping', 'DomesticShippingCrudController');
CRUD::resource('overseas_shipping', 'OverseasShippingCrudController');