<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('lang/{locale}', function (Request $request) {
	if($request->path() == 'lang/en')
	{
	   Session::put('locale', 'en');
	}
	elseif($request->path() == 'lang/ja')
	{
		Session::put('locale', 'ja');
	}
	return redirect(URL::previous());
});

Route::auth();

Route::get('loginpage','General@login');
Route::post('loginprocess','General@loginprocess');

Route::get('cron_email', 'EmailController@sentMails');

Route::get('/logout', function(){
    Auth::logout();
    Session::flush();
    return redirect('/');
});

Route::post('downloadcsv/domestic', 'Domestic@downloadCSV');
Route::post('downloadcsv/international', 'International@downloadCSV');
Route::get('csv/{filename}', 'General@indexDownloadCsv');

Route::group(['middleware' => ['auth']],function(){
	Route::get('/', 'General@indexHomepage');

	Route::get('selectshop', 'ShopController@indexSelectShop');
	Route::post('saveselectedshop', 'ShopController@saveSelectedShop');
	Route::post('newshop', 'ShopController@addNewShop');

	Route::get('slip/{shipping_code}', 'General@indexPrintSlip');
	Route::post('saveorder', 'General@saveOrder');

	Route::get('/profile', 'General@indexProfile');
	Route::post('/profile/edit', 'General@editProfile');
	Route::post('checkpassword', 'General@checkPassword');

	Route::get('getShop','ShopController@getShop');

	Route::get('/myshipments', function(){
		return view('myshipments');
	});
	Route::get('/getmyshipments','General@myShipments');
	Route::post('/deleteShipment/{type}/{id}','General@deleteShipment');
	Route::get('/page/{slug}','General@getPagecontent');

	//DOMESTIC
	Route::get('domestic_rule','Domestic@rule');
	Route::get('domestic_area','Domestic@areainfo');
	Route::get('size_dom', 'Domestic@setItemSizeWeight');
	Route::post('get_size_dom', 'Domestic@getItemSizeWeight');
	Route::get('domestic_content_item', 'Domestic@setContentItems');
	Route::post('dom_get_total','Domestic@getContentItems');
	Route::get('domestic_destination_address', 'Domestic@setDestination');
	Route::post('getAirportCoordinate','Domestic@getAirportCoordinate');
	Route::post('dom_get_address_data','Domestic@getDestination');
	Route::get('domestic_shipping', 'Domestic@setPaymentMethod');
	Route::post('dom_getpaymentmethod','Domestic@getPaymentMethod');
	Route::get('creditcard_payment','Domestic@setCreditCardInfo');
	Route::post('get_cc_info','Domestic@getCreditCardInfo');
	Route::get('wechat_payment','Domestic@setWechatPayment');
	Route::get('alipay_payment','Domestic@setAlipayPayment');
	Route::get('unionpay_payment','Domestic@setUnionpayPayment');
	Route::get('payment_complete','Domestic@payment_complete');

	Route::get('domestic/{shopid}', 'Domestic@index');
	Route::get('domestic', function(){
		return redirect('/');
	});
	Route::post('getshippingcost', 'Domestic@getShippingCost');

	//INTERNATIONAL
	Route::get('/international_rule','International@rule');
	Route::get('/ems','International@ems');
	Route::get('/size_int', 'International@setItemSizeWeight');
	Route::post('/get_size_int', 'International@getItemSizeWeight');
	Route::get('/international_content_item', 'International@setContentItems');
	Route::post('/int_get_total','International@getContentItems');
	Route::get('/international_destination_address', 'International@setDestination');
	Route::post('/int_get_address_data','International@getDestination');
	Route::get('/international_shipping', 'International@setPaymentMethod');
	Route::post('/int_getpaymentmethod','International@getPaymentMethod');

	Route::get('international/{shopid}', 'International@index');
	Route::get('international', function(){
		return redirect('/');
	});
	Route::post('international_cost_reference', 'International@getInternationalShippingCostReference');
	Route::post('getinternationalshippingcost', 'International@getShippingCost');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tes', function(){
	return view('tes');
});