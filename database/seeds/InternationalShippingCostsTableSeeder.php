<?php

use Illuminate\Database\Seeder;

class InternationalShippingCostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('international_shipping_costs')->insert(
        	[	
        		// Asia
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "1", 'price' => "6000"],
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "0", 'price' => "6540"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "1", 'price' => "9800"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "0", 'price' => "10340"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "1", 'price' => "13500"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "0", 'price' => "14040"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "1", 'price' => "17100"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "0", 'price' => "17640"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "1", 'price' => "20700"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "0", 'price' => "21240"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "1", 'price' => "24400"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "asia", 'packing_status' => "0", 'price' => "24940"],

            	// Australia
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "australia", 'packing_status' => "0", 'price' => "0"],

            	// Europe
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "1", 'price' => "9200"],
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "0", 'price' => "9740"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "1", 'price' => "15400"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "0", 'price' => "15940"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "1", 'price' => "21300"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "0", 'price' => "21840"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "1", 'price' => "27200"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "0", 'price' => "27740"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "1", 'price' => "33100"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "0", 'price' => "33640"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "1", 'price' => "39100"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "europe", 'packing_status' => "0", 'price' => "39640"],

            	// Africa
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "africa", 'packing_status' => "0", 'price' => "0"],

            	// North America
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "1", 'price' => "8200"],
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "0", 'price' => "8740"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "1", 'price' => "13500"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "0", 'price' => "14040"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "1", 'price' => "18500"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "0", 'price' => "19040"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "1", 'price' => "23500"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "0", 'price' => "24040"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "1", 'price' => "28500"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "0", 'price' => "29040"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "1", 'price' => "33500"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "north_america", 'packing_status' => "0", 'price' => "34040"],

            	// South America
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "5", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "10", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "15", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "20", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "25", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "0", 'price' => "0"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "1", 'price' => "0"],
            	['weight' => "30", 'length' => "180", 'totalsize' => "300", 'area_id' => "south_america", 'packing_status' => "0", 'price' => "0"],
        	]
        );
    }
}
