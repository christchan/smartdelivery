<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(ShippingAreasTableSeeder::class);
        $this->call(AirportsTableSeeder::class);
        $this->call(PrefecturesTableSeeder::class);
        $this->call(PrefectureShippingAreasTableSeeder::class);
        $this->call(ShippingCostsTableSeeder::class);
        $this->call(ContentItemsTableSeeder::class);
        $this->call(InternationalShippingCostsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(PageTableSeeder::class);
        factory(App\Shop::class, 20)->create();
        // factory(App\User::class, 10)->create();
    }
}
