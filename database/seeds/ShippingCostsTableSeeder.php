<?php

use Illuminate\Database\Seeder;

class ShippingCostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipping_costs')->insert(
        	[
            	['weight' => "2", 'length' => "0", 'totalsize' => "60", 'area_id' => "3", 'packing_status' => "1", 'price' => "864",],
            	['weight' => "2", 'length' => "0", 'totalsize' => "60", 'area_id' => "3", 'packing_status' => "0", 'price' => "1188",],
                ['weight' => "2", 'length' => "0", 'totalsize' => "60", 'area_id' => "1", 'packing_status' => "1", 'price' => "1188",],
                ['weight' => "2", 'length' => "0", 'totalsize' => "60", 'area_id' => "1", 'packing_status' => "0", 'price' => "1512",],
                ['weight' => "2", 'length' => "0", 'totalsize' => "60", 'area_id' => "2", 'packing_status' => "1", 'price' => "1512",],
                ['weight' => "2", 'length' => "0", 'totalsize' => "60", 'area_id' => "2", 'packing_status' => "0", 'price' => "1836",],
                ['weight' => "5", 'length' => "0", 'totalsize' => "80", 'area_id' => "3", 'packing_status' => "1", 'price' => "972",],
                ['weight' => "5", 'length' => "0", 'totalsize' => "80", 'area_id' => "3", 'packing_status' => "0", 'price' => "1296",],
                ['weight' => "5", 'length' => "0", 'totalsize' => "80", 'area_id' => "1", 'packing_status' => "1", 'price' => "1188",],
                ['weight' => "5", 'length' => "0", 'totalsize' => "80", 'area_id' => "1", 'packing_status' => "0", 'price' => "1512",],
                ['weight' => "5", 'length' => "0", 'totalsize' => "80", 'area_id' => "2", 'packing_status' => "1", 'price' => "1620",],
                ['weight' => "5", 'length' => "0", 'totalsize' => "80", 'area_id' => "2", 'packing_status' => "0", 'price' => "1944",],
                ['weight' => "10", 'length' => "0", 'totalsize' => "100", 'area_id' => "3", 'packing_status' => "1", 'price' => "1080",],
                ['weight' => "10", 'length' => "0", 'totalsize' => "100", 'area_id' => "3", 'packing_status' => "0", 'price' => "1404",],
                ['weight' => "10", 'length' => "0", 'totalsize' => "100", 'area_id' => "1", 'packing_status' => "1", 'price' => "1296",],
                ['weight' => "10", 'length' => "0", 'totalsize' => "100", 'area_id' => "1", 'packing_status' => "0", 'price' => "1620",],
                ['weight' => "10", 'length' => "0", 'totalsize' => "100", 'area_id' => "2", 'packing_status' => "1", 'price' => "1728",],
                ['weight' => "10", 'length' => "0", 'totalsize' => "100", 'area_id' => "2", 'packing_status' => "0", 'price' => "2052",],
                ['weight' => "15", 'length' => "0", 'totalsize' => "120", 'area_id' => "3", 'packing_status' => "1", 'price' => "1080",],
                ['weight' => "15", 'length' => "0", 'totalsize' => "120", 'area_id' => "3", 'packing_status' => "0", 'price' => "1404",],
                ['weight' => "15", 'length' => "0", 'totalsize' => "120", 'area_id' => "1", 'packing_status' => "1", 'price' => "1296",],
                ['weight' => "15", 'length' => "0", 'totalsize' => "120", 'area_id' => "1", 'packing_status' => "0", 'price' => "1620",],
                ['weight' => "15", 'length' => "0", 'totalsize' => "120", 'area_id' => "2", 'packing_status' => "1", 'price' => "1728",],
                ['weight' => "15", 'length' => "0", 'totalsize' => "120", 'area_id' => "2", 'packing_status' => "0", 'price' => "2052",],
                ['weight' => "20", 'length' => "0", 'totalsize' => "140", 'area_id' => "3", 'packing_status' => "1", 'price' => "1080",],
                ['weight' => "20", 'length' => "0", 'totalsize' => "140", 'area_id' => "3", 'packing_status' => "0", 'price' => "1404",],
                ['weight' => "20", 'length' => "0", 'totalsize' => "140", 'area_id' => "1", 'packing_status' => "1", 'price' => "1296",],
                ['weight' => "20", 'length' => "0", 'totalsize' => "140", 'area_id' => "1", 'packing_status' => "0", 'price' => "1620",],
                ['weight' => "20", 'length' => "0", 'totalsize' => "140", 'area_id' => "2", 'packing_status' => "1", 'price' => "1728",],
                ['weight' => "20", 'length' => "0", 'totalsize' => "140", 'area_id' => "2", 'packing_status' => "0", 'price' => "2052",],
                ['weight' => "25", 'length' => "0", 'totalsize' => "160", 'area_id' => "3", 'packing_status' => "1", 'price' => "1080",],
                ['weight' => "25", 'length' => "0", 'totalsize' => "160", 'area_id' => "3", 'packing_status' => "0", 'price' => "1404",],
                ['weight' => "25", 'length' => "0", 'totalsize' => "160", 'area_id' => "1", 'packing_status' => "1", 'price' => "1296",],
                ['weight' => "25", 'length' => "0", 'totalsize' => "160", 'area_id' => "1", 'packing_status' => "0", 'price' => "1620",],
                ['weight' => "25", 'length' => "0", 'totalsize' => "160", 'area_id' => "2", 'packing_status' => "1", 'price' => "1728",],
                ['weight' => "25", 'length' => "0", 'totalsize' => "160", 'area_id' => "2", 'packing_status' => "0", 'price' => "2052",],

        	]
        );
    }
}
