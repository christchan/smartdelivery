<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        [
            [
                'name' => str_random(10),
                'email' => 'admin@user.com',
                'password' => bcrypt('admin'),
            ],
            [
                'name' => str_random(10),
                'email' => 'user@user.com',
                'password' => bcrypt('user'),
            ],
            [
                'name' => str_random(10),
                'email' => 'test1@test.com',
                'password' => bcrypt('test'),
            ],
        ]);

        DB::table('user_has_roles')->insert(
            [
                ['role_id' => 1, 'user_id' => 1],
                ['role_id' => 2, 'user_id' => 2],
                ['role_id' => 2, 'user_id' => 3],
            ]
        );
    }
}
