<?php

use Illuminate\Database\Seeder;

class ContentItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('content_items')->insert(
        	[
            	['name' => "衣服", 'lang_id' => "1"],
            	['name' => "clothes", 'lang_id' => "2"],
            	['name' => "衣服", 'lang_id' => "3"],
            	['name' => "衣服", 'lang_id' => "4"],

            	['name' => "身の回り品", 'lang_id' => "1"],
            	['name' => "personal effect", 'lang_id' => "2"],
            	['name' => "随身物品", 'lang_id' => "3"],
            	['name' => "随身物品", 'lang_id' => "4"],

                ['name' => "衣服", 'lang_id' => "1"],
                ['name' => "clothes", 'lang_id' => "2"],
                ['name' => "衣服", 'lang_id' => "3"],
                ['name' => "衣服", 'lang_id' => "4"],

                ['name' => "履物", 'lang_id' => "1"],
                ['name' => "footwear", 'lang_id' => "2"],   
                ['name' => "鞋", 'lang_id' => "3"],
                ['name' => "鞋", 'lang_id' => "4"],

                ['name' => "化粧品・歯みがき・石けん・家庭用洗剤", 'lang_id' => "1"],
                ['name' => "cosmetics", 'lang_id' => "2"],   
                ['name' => "化妆品・牙膏・肥皂・家用洗涤剂", 'lang_id' => "3"],
                ['name' => "化妝品・牙膏・肥皂・家用洗滌劑", 'lang_id' => "4"],

                ['name' => "医薬品", 'lang_id' => "1"],  
                ['name' => "drug medicine", 'lang_id' => "2"],   
                ['name' => "医药品", 'lang_id' => "3"], 
                ['name' => "醫藥品", 'lang_id' => "4"],

                ['name' => "その他の住生活用品", 'lang_id' => "1"],
                ['name' => "living goods", 'lang_id' => "2"],    
                ['name' => " 其他的住生活用品", 'lang_id' => "3"],    
                ['name' => " 其他的住生活用品", 'lang_id' => "4"],

                ['name' => "スポーツ用具", 'lang_id' => "1"],
                ['name' => "sports goods", 'lang_id' => "2"],    
                ['name' => "体育用具", 'lang_id' => "3"],    
                ['name' => "體育用具", 'lang_id' => "4"],

                ['name' => "時計", 'lang_id' => "1"],  
                ['name' => "watch", 'lang_id' => "2"],   
                ['name' => "表", 'lang_id' => "3"],   
                ['name' => "表", 'lang_id' => "4"],

                ['name' => "民生用電気・電子製品", 'lang_id' => "1"],
                ['name' => "home electronic product", 'lang_id' => "2"], 
                ['name' => "民生用电气・电子产品", 'lang_id' => "3"],  
                ['name' => "民生用電氣・電子產品", 'lang_id' => "4"],

                ['name' => "文具・事務用具", 'lang_id' => "1"], 
                ['name' => "stationery", 'lang_id' => "2"],  
                ['name' => "文具・事务用具", 'lang_id' => "3"], 
                ['name' => "文具・事務用具", 'lang_id' => "4"],

                ['name' => "台所用品", 'lang_id' => "1"],    
                ['name' => "kitchen ware", 'lang_id' => "2"],    
                ['name' => "厨房用品", 'lang_id' => "3"],    
                ['name' => "廚房用品", 'lang_id' => "4"],

                ['name' => "美術品・収集品", 'lang_id' => "1"], 
                ['name' => "art object", 'lang_id' => "2"],  
                ['name' => "美术品・收集品", 'lang_id' => "3"], 
                ['name' => "美術品・收集品", 'lang_id' => "4"],
        	]
        );
    }
}
