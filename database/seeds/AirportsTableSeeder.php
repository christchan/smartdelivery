<?php

use Illuminate\Database\Seeder;

class AirportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('airports')->insert(
        	[
            	['name' => "成田空港/第1ターミナル（北ウイング）", 'postcode' => "282-0011", 'address' => "Chiba,    Narita-City,Sanrizuka-Goryobokujyo,1-1,Narita Airport TERMINAL1 NORTH WING", 'tel' => "0476-32-4755", 'latitude' => "35.7715602", 'longitude' => "140.3789883", 'lang_id' => "1",],
            	['name' => "Narita International Airport Terminal1 North Wing", 'postcode' => "282-0011", 'address' => "Chiba,    Narita-City,Sanrizuka-Goryobokujyo,1-1,Narita Airport TERMINAL1 NORTH WING", 'tel' => "0476-32-4755", 'latitude' => "35.7715602", 'longitude' => "140.3789883", 'lang_id' => "2",],
            	['name' => "成田空港/第1ターミナル（南ウイング）", 'postcode' => "282-0011", 'address' => "Chiba,    Narita-City,Sanrizuka-Goryobokujyo,1-1,Narita Airport TERMINAL1 SOUTH WING", 'tel' => "0476-32-4755", 'latitude' => "35.7715602", 'longitude' => "140.3789883", 'lang_id' => "1",],
            	['name' => "Narita International Airport Terminal1 South Wing", 'postcode' => "282-0011", 'address' => "Chiba,    Narita-City,Sanrizuka-Goryobokujyo,1-1,Narita Airport TERMINAL1 SOUTH WING", 'tel' => "0476-32-4755", 'latitude' => "35.7715602", 'longitude' => "140.3789883", 'lang_id' => "2",],
            	['name' => "成田空港/第2ターミナル", 'postcode' => "282-0004", 'address' => "Chiba,    Narita-City,Furugome,1-1,Narita Airport TERMINAL2", 'tel' => "0476-32-4755", 'latitude' => "35.77327", 'longitude' => "140.3854113", 'lang_id' => "1",],
            	['name' => "Narita International Airport Terminal2", 'postcode' => "282-0011", 'address' => "Chiba,    Narita-City,Furugome,1-1,Narita Airport TERMINAL2", 'tel' => "0476-32-4755", 'latitude' => "35.77327", 'longitude' => "140.3854113", 'lang_id' => "2",],
            	['name' => "羽田空港/（国際線）", 'postcode' => "144-0041", 'address' => "Tokyo,  Ohta-ku, Haneda Airport INTERNATIONAL TERMINAL", 'tel' => "03-6756-7174", 'latitude' => "35.5493932", 'longitude' => "139.7776499", 'lang_id' => "1",],
            	['name' => "Tokyo International Airport", 'postcode' => "144-0041", 'address' => "Tokyo,  Ohta-ku, Haneda Airport INTERNATIONAL TERMINAL", 'tel' => "03-6756-7174", 'latitude' => "35.5493932", 'longitude' => "139.7776499", 'lang_id' => "2",],
            	['name' => "中部国際空港", 'postcode' => "479-0881", 'address' => "Aichi,   Tokoname-City,Centrair,1-1, Chubu Centrair International Airport", 'tel' => "0569-38-8562", 'latitude' => "34.8590935", 'longitude' => "136.8124118", 'lang_id' => "1",],
            	['name' => "Chubu Centrair International Airport", 'postcode' => "479-0881", 'address' => "Aichi,   Tokoname-City,Centrair,1-1, Chubu Centrair International Airport", 'tel' => "0569-38-8562", 'latitude' => "34.8590935", 'longitude' => "136.8124118", 'lang_id' => "2",],
            	['name' => "関西国際空港/第1ターミナルビル", 'postcode' => "549-0001", 'address' => "Osaka,   Izumisano-City,Senshu-Kukokita,1,Kansai International Airport Terminal1", 'tel' => "072-461-3123", 'latitude' => "34.4320024", 'longitude' => "135.2282052", 'lang_id' => "1",],
            	['name' => "Kansai International Airport Terminal1", 'postcode' => "549-0001", 'address' => "Osaka,   Izumisano-City,Senshu-Kukokita,1,Kansai International Airport Terminal1", 'tel' => "072-461-3123", 'latitude' => "34.4320024", 'longitude' => "135.2282052", 'lang_id' => "2",],
        	]
        );
    }
}
