<?php

use Illuminate\Database\Seeder;

class PrefecturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prefectures')->insert(
        	[
            	['name' => "愛知県", 'lang_id' => "1"],
            	['name' => "秋田県", 'lang_id' => "1"],
            	['name' => "青森県", 'lang_id' => "1"],
                ['name' => "千葉県", 'lang_id' => "1"],
                ['name' => "愛媛県", 'lang_id' => "1"],
                ['name' => "福井県", 'lang_id' => "1"],
                ['name' => "福島県", 'lang_id' => "1"],
                ['name' => "岐阜県", 'lang_id' => "1"],
                ['name' => "群馬県", 'lang_id' => "1"],
                ['name' => "広島県", 'lang_id' => "1"],
                ['name' => "兵庫県", 'lang_id' => "1"],
                ['name' => "茨城県", 'lang_id' => "1"],
                ['name' => "石川県", 'lang_id' => "1"],
                ['name' => "岩手県", 'lang_id' => "1"],
                ['name' => "香川県", 'lang_id' => "1"],
                ['name' => "神奈川県", 'lang_id' => "1"],
                ['name' => "高知県", 'lang_id' => "1"],
                ['name' => "京都府", 'lang_id' => "1"],
                ['name' => "三重県", 'lang_id' => "1"],
                ['name' => "宮城県", 'lang_id' => "1"],
                ['name' => "長野県", 'lang_id' => "1"],
                ['name' => "奈良県", 'lang_id' => "1"],
                ['name' => "新潟県", 'lang_id' => "1"],
                ['name' => "岡山県", 'lang_id' => "1"],
                ['name' => "大阪府", 'lang_id' => "1"],
                ['name' => "埼玉県", 'lang_id' => "1"],
                ['name' => "滋賀県", 'lang_id' => "1"],
                ['name' => "島根県", 'lang_id' => "1"],
                ['name' => "静岡県", 'lang_id' => "1"],
                ['name' => "栃木県", 'lang_id' => "1"],
                ['name' => "徳島県", 'lang_id' => "1"],
                ['name' => "東京都", 'lang_id' => "1"],
                ['name' => "鳥取県", 'lang_id' => "1"],
                ['name' => "富山県", 'lang_id' => "1"],
                ['name' => "和歌山県", 'lang_id' => "1"],
                ['name' => "山形県", 'lang_id' => "1"],
                ['name' => "山口県", 'lang_id' => "1"],
                ['name' => "山梨県", 'lang_id' => "1"],
                ['name' => "Aichi", 'lang_id' => "2"],
                ['name' => "Akita", 'lang_id' => "2"],
                ['name' => "Aomori", 'lang_id' => "2"],
                ['name' => "Chiba", 'lang_id' => "2"],
                ['name' => "Ehime", 'lang_id' => "2"],
                ['name' => "Fukui", 'lang_id' => "2"],
                ['name' => "Fukushima", 'lang_id' => "2"],
                ['name' => "Gifu", 'lang_id' => "2"],
                ['name' => "Gunma", 'lang_id' => "2"],
                ['name' => "Hiroshima", 'lang_id' => "2"],
                ['name' => "Hyogo", 'lang_id' => "2"],
                ['name' => "Ibaraki", 'lang_id' => "2"],
                ['name' => "Ishikawa", 'lang_id' => "2"],
                ['name' => "Iwate", 'lang_id' => "2"],
                ['name' => "Kagawa", 'lang_id' => "2"],
                ['name' => "Kanagawa", 'lang_id' => "2"],
                ['name' => "Kochi", 'lang_id' => "2"],
                ['name' => "Kyoto", 'lang_id' => "2"],
                ['name' => "Mie", 'lang_id' => "2"],
                ['name' => "Miyagi", 'lang_id' => "2"],
                ['name' => "Nagano", 'lang_id' => "2"],
                ['name' => "Nara", 'lang_id' => "2"],
                ['name' => "Niigata", 'lang_id' => "2"],
                ['name' => "Okayama", 'lang_id' => "2"],
                ['name' => "Osaka", 'lang_id' => "2"],
                ['name' => "Saitama", 'lang_id' => "2"],
                ['name' => "Shiga", 'lang_id' => "2"],
                ['name' => "Shimane", 'lang_id' => "2"],
                ['name' => "Shizuoka", 'lang_id' => "2"],
                ['name' => "Tochigi", 'lang_id' => "2"],
                ['name' => "Tokushima", 'lang_id' => "2"],
                ['name' => "Tokyo", 'lang_id' => "2"],
                ['name' => "Tottori", 'lang_id' => "2"],
                ['name' => "Toyama", 'lang_id' => "2"],
                ['name' => "Wakayama", 'lang_id' => "2"],
                ['name' => "Yamagata", 'lang_id' => "2"],
                ['name' => "Yamaguchi", 'lang_id' => "2"],
                ['name' => "Yamanashi", 'lang_id' => "2"],
        	]
        );
    }
}
