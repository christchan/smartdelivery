<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert(
        	[
            	['name' => "Japanese"],
            	['name' => "English"],
            	['name' => "Simplified Chinese"],
            	['name' => "Traditional Chinese"],
        	]
        );
    }
}
