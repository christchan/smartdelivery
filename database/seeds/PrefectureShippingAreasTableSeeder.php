<?php

use Illuminate\Database\Seeder;

class PrefectureShippingAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prefecture_shipping_areas')->insert(
            [
                ['pref_id' => "62"],
                ['pref_id' => "48"],
                ['pref_id' => "75"],
                ['pref_id' => "71"],
                ['pref_id' => "66"],
                ['pref_id' => "53"],
                ['pref_id' => "69"],
                ['pref_id' => "55"],
                ['pref_id' => "43"],

                ['pref_id' => "24"],
                ['pref_id' => "10"],
                ['pref_id' => "37"],
                ['pref_id' => "33"],
                ['pref_id' => "28"],
                ['pref_id' => "15"],
                ['pref_id' => "31"],
                ['pref_id' => "17"],
                ['pref_id' => "5"],
            ]
        );
    }
}
