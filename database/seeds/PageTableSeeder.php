<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert(
        	[
            	[
            		'slug' => "aboutus",
            		"title" => 'About Us',
            		'content' => 'Lorem ipsum dolor sit amet, in est albucius liberavisse complectitur, ea qui primis dictas imperdiet, vim nostrum oportere adolescens at. Case nostrum mentitum eu qui, te mea zril nusquam lobortis, usu vocibus recusabo ea. Probatus perpetua ea mea, qui homero invenire in. Nam ad nibh virtute, has ex quod paulo pertinax, nam eu vero eligendi. Tale aliquam atomorum sea id. Ne mea etiam doming, partem invenire facilisis ea eos. Quis gubergren cu sit, et vix meliore noluisse. Fugit epicurei eum ut, vim alia reprimique ea. Dicam euismod theophrastus an his. Quo eleifend qualisque ad, per in duis debitis adversarium, usu ut primis facilisis suavitate. Eam graeco dicunt mediocrem ea, cibo intellegebat ei mel, vel quodsi vocibus ex.

						Quot virtute incorrupte an vix. Timeam integre ancillae no nec. At voluptua oporteat per. Vim zril explicari ut.

						Nam luptatum efficiendi in, ut eam tamquam adolescens, sit cu vide ridens nominavi. Probo assueverit ne nec, summo incorrupte reprimique mea at. Alia probo omnis cu usu, duo integre bonorum no, sea quando viderer qualisque ne. Ex nisl doctus his, ea veritus delectus ius. Mei hinc meliore voluptatibus ex.

						Eu qui omnium fastidii molestiae, eam an iisque ornatus temporibus, id eum altera eruditi appetere. Vel id decore praesent. At posse sonet pri, pri ludus persecuti vituperata ad. Vim cu tibique commune, ne eam movet aeterno impedit, quo nisl stet omnes ad. Ne elit electram voluptatum nec.'
            	],
            	[
            		'slug' => "help",
            		"title" => 'Help',
            		'content' => 'Lorem ipsum dolor sit amet, in est albucius liberavisse complectitur, ea qui primis dictas imperdiet, vim nostrum oportere adolescens at. Case nostrum mentitum eu qui, te mea zril nusquam lobortis, usu vocibus recusabo ea. Probatus perpetua ea mea, qui homero invenire in. Nam ad nibh virtute, has ex quod paulo pertinax, nam eu vero eligendi. Tale aliquam atomorum sea id. Ne mea etiam doming, partem invenire facilisis ea eos. Quis gubergren cu sit, et vix meliore noluisse. Fugit epicurei eum ut, vim alia reprimique ea. Dicam euismod theophrastus an his. Quo eleifend qualisque ad, per in duis debitis adversarium, usu ut primis facilisis suavitate. Eam graeco dicunt mediocrem ea, cibo intellegebat ei mel, vel quodsi vocibus ex.

						Quot virtute incorrupte an vix. Timeam integre ancillae no nec. At voluptua oporteat per. Vim zril explicari ut.

						Nam luptatum efficiendi in, ut eam tamquam adolescens, sit cu vide ridens nominavi. Probo assueverit ne nec, summo incorrupte reprimique mea at. Alia probo omnis cu usu, duo integre bonorum no, sea quando viderer qualisque ne. Ex nisl doctus his, ea veritus delectus ius. Mei hinc meliore voluptatibus ex.

						Eu qui omnium fastidii molestiae, eam an iisque ornatus temporibus, id eum altera eruditi appetere. Vel id decore praesent. At posse sonet pri, pri ludus persecuti vituperata ad. Vim cu tibique commune, ne eam movet aeterno impedit, quo nisl stet omnes ad. Ne elit electram voluptatum nec.'
            	]
            	
        	]
        );
    }
}
