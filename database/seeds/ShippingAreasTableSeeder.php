<?php

use Illuminate\Database\Seeder;

class ShippingAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipping_areas')->insert(
        	[
            	['name' => "Prefecture"],
            	['name' => "Airport"],
            	['name' => "Other"],
            	['name' => "Asia"],
            	['name' => "America"],
            	['name' => "Europe"],
        	]
        );
    }
}
