<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerEmailFieldToDomesticAndOverseasShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domestic_shippings', function (Blueprint $table) {
            $table->string('customer_email')->after('customer_name');
        });
        Schema::table('overseas_shippings', function (Blueprint $table) {
            $table->string('customer_email')->after('customer_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domestic_shippings');
        Schema::dropIfExists('overseas_shippings');
    }
}
