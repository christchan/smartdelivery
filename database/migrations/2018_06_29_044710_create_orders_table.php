<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('shipping_code');
            $table->datetime('order_date');
            $table->text('destination_address');
            $table->string('destination_tel');
            $table->string('shop_name');
            $table->integer('parcel_size');
            $table->integer('parcel_weight');
            $table->string('packing_status');
            $table->text('contents');
            $table->integer('total_shipping_cost');
            $table->string('stripe_charge_id');
            $table->string('shipping_type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
