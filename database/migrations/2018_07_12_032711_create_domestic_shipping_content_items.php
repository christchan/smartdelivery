<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomesticShippingContentItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domestic_shipping_content_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipping_id');
            $table->integer('content_item');
            $table->integer('price');
            $table->integer('piece');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domestic_shipping_content_items');
    }
}
