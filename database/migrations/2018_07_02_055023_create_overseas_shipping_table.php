<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverseasShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overseas_shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('record_id');
            $table->string('shipping_way_code')->default("111");
            $table->string('customer_name')->default("");
            $table->text('customer_other_address');
            $table->text('customer_other_address2');
            $table->string('customer_city')->default("");
            $table->string('customer_state')->default("");
            $table->string('customer_post_code')->default("");
            $table->string('customer_tel')->default("");
            $table->string('customer_country')->default("");
            $table->string('remarks')->default("");
            $table->string('shipping_company_code')->default("00000005");
            $table->integer('product_type')->default(5);
            $table->integer('total_price')->default(0);
            $table->integer('total_weight')->default(0);
            $table->string('item1_code')->nullable();
            $table->string('item1_name')->default("");
            $table->string('item1_hs_code')->nullable();
            $table->string('item1_made_country_code')->nullable();
            $table->integer('item1_pieces')->default(0);
            $table->integer('item1_unit_price')->default(0);
            $table->integer('item1_weight')->default(0);
            $table->string('item2_code')->nullable();
            $table->string('item2_name')->default("");
            $table->string('item2_hs_code')->nullable();
            $table->string('item2_made_country_code')->nullable();
            $table->integer('item2_pieces')->default(0);
            $table->integer('item2_unit_price')->default(0);
            $table->integer('item2_weight')->default(0);
            $table->string('item3_code')->nullable();
            $table->string('item3_name')->default("");
            $table->string('item3_hs_code')->nullable();
            $table->string('item3_made_country_code')->nullable();
            $table->integer('item3_pieces')->default(0);
            $table->integer('item3_unit_price')->default(0);
            $table->integer('item3_weight')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overseas_shippings');
    }
}
