<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldsToOverseasShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('overseas_shippings', function (Blueprint $table) {
            $table->string('payment_method')->after('shipping_way_code');
            $table->integer('item_total_qty')->after('payment_method');
            $table->integer('item_grand_total')->after('item_total_qty');
            $table->integer('packing_status')->after('item_grand_total');
            $table->integer('total_size')->after('total_weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overseas_shippings');
    }
}
