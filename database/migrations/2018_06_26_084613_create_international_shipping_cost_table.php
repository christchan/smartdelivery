<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternationalShippingCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('international_shipping_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('weight');
            $table->integer('totalsize');
            $table->integer('length');
            $table->tinyInteger('packing_status');
            $table->string('area_id');
            $table->double('price');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('international_shipping_costs');
    }
}
