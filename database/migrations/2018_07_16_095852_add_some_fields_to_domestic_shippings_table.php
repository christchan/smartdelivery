<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldsToDomesticShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domestic_shippings', function (Blueprint $table) {
            $table->string('payment_method')->after('payment_type');
            $table->integer('item_total_qty')->after('payment_method');
            $table->integer('item_grand_total')->after('item_total_qty');
            $table->integer('packing_status')->after('item_grand_total');
            $table->integer('total_weight')->after('packing_status');
            $table->integer('total_size')->after('total_weight');
            $table->integer('total_price')->after('total_size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domestic_shippings');
    }
}
