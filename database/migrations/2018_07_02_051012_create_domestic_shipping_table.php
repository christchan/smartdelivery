<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomesticShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domestic_shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('record_id');
            $table->integer('payment_type')->default(0);
            $table->integer('cool_type')->default(0);
            $table->string('slip_id')->default("");
            $table->datetime('shipping_date');
            $table->datetime('delivery_date');
            $table->string('delivery_time_range')->nullable();
            $table->string('delivery_code')->nullable();
            $table->string('customer_tel')->default("");
            $table->string('customer_tel_edaban')->nullable();
            $table->string('customer_post_code')->default("");
            $table->text('customer_address1');
            $table->text('customer_address2');
            $table->string('company_name')->nullable();
            $table->string('company_division')->nullable();
            $table->string('customer_name')->default("");
            $table->string('customer_name_kana')->nullable();
            $table->string('customer_sir')->nullable();
            $table->string('agent_code')->nullable();
            $table->string('agent_tel')->default("090-4929-4510");
            $table->string('agent_tel_edaban')->nullable();
            $table->string('agent_post_code')->default("160-0023");
            $table->string('agent_address1')->default("東京都新宿区西新宿8-3-1");
            $table->text('agent_address2');
            $table->string('agent_name')->default("（株）セルハート");
            $table->string('agent_name_kana')->nullable();
            $table->string('item1_code')->nullable();
            $table->string('item1_name')->default("");
            $table->string('item2_code')->nullable();
            $table->string('item2_name')->nullable();
            $table->string('item1_atsukai')->nullable();
            $table->string('item2_atsukai')->nullable();
            $table->string('remarks')->default("");
            $table->integer('collect_payment_amount')->nullable();
            $table->integer('collect_payment_tax')->nullable();
            $table->integer('eigyo_stop')->default(0);
            $table->string('eigyo_code')->nullable();
            $table->string('hakko_maisu')->nullable();
            $table->integer('kosu_flag')->default(2);
            $table->string('others1')->nullable();
            $table->string('others2')->nullable();
            $table->string('others3')->nullable();
            $table->string('others4')->nullable();
            $table->string('others5')->nullable();
            $table->string('others6')->nullable();
            $table->string('others7')->nullable();
            $table->string('others8')->nullable();
            $table->string('others9')->nullable();
            $table->string('others10')->nullable();
            $table->string('others11')->nullable();
            $table->string('others12')->nullable();
            $table->string('others13')->nullable();
            $table->string('others14')->nullable();
            $table->string('others15')->nullable();
            $table->string('others16')->nullable();
            $table->string('others17')->nullable();
            $table->string('others18')->nullable();
            $table->string('others19')->nullable();
            $table->string('others20')->nullable();
            $table->string('others21')->nullable();
            $table->string('others22')->nullable();
            $table->string('others23')->nullable();
            $table->string('others24')->nullable();
            $table->string('others25')->nullable();
            $table->string('others26')->nullable();
            $table->string('others27')->nullable();
            $table->string('others28')->nullable();
            $table->string('others29')->nullable();
            $table->string('others30')->nullable();
            $table->string('others31')->nullable();
            $table->string('others32')->nullable();
            $table->string('others33')->nullable();
            $table->string('others34')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domestic_shippings');
    }
}
