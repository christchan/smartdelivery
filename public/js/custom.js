$(document).ready( function () {
    selectaddress();

    function selectaddress()
    {
        var delivery_type_id = $('.deliveryselection:checked').val();
        var request_switchon = $('.deliveryselection:checked').data('switchon');
        if (request_switchon == 1) {
            $('.domestic-airport-form').show();
            $('.domestic-address-form').hide();
        }
        else{
            $('.domestic-airport-form').hide();
            $('.domestic-address-form').show();
        } 
    }

    $('.selectdelivery').change(function(){
        selectaddress();
    });
    
    $("#login").click(function(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: url + "/loginprocess",
            data: { 
                id: $('#id').val(),
                password: $('#password').val()
            },
            data: $("#form").serialize(),
            success: function(data) {
                if(data.response == true)
                    window.location.href = url;
                else
                    alert("Email/password wrong");
            },
            error: function(response) {
                console.log('Error:', response.responseText);
            }
        });
        
    });
    
    $( "#next" ).click(function() {
        //console.log($(this).prop("value"));
        if($(this).prop("value") == "dom_step1")
        {
            window.location.replace('domestic_area');
        }
        else if($(this).prop("value") == "dom_step2")
        {
            window.location.replace('size_dom');
        }
        else if($(this).prop("value") == "int_step1")
        {
            window.location.replace('ems');
        }
        else if($(this).prop("value") == "int_step2")
        {
            window.location.replace('size_int');
        }
        else if($(this).prop("value") == "step3")
        {
            //console.log($('.deliveryselection:checked').val());
            var size = $('#size').val();
            var weight = $('#weight').val();
            
            if(size.length>0 && weight.length>0)
            {
                $("#form").submit();
            }
            else
            {
                alert("Field cannot be empty");
            }
        }
        else if($(this).prop("value") == "step4")
        {
            var grandtotal = $('#grandtotal').val();
            
            if(grandtotal.length>0)
            {
                $("#form").submit();
            }
            else
            {
                alert("Field cannot be empty");
            }
        }
        else if($(this).prop("value") == "step5")
        {
            $("#form").submit();
            /*
            var ds = $('.deliveryselection:checked').val();

            if(ds == 0) //address
            {
                /*
                $address = $request->address;
                $postcode = $request->postcode;
                $phone = $request->phone;
                $name = $request->name;
                $email = $request->email;
                $delivery_date = $request->delivery_date;
            }
            else if(ds == 1) //airport
            {
                
            }
            */
        }
        else if($(this).prop("value") == "step6")
        {
            var radio = $('.payment:checked').val();

            if(radio == "Credit Card")
            {
                var info = $('#stripeToken').val();
                if(info.length>0)
                {
                    $("#form").submit();
                }
                else
                {
                    alert("Please fill payment info");
                }
            }
        }
        else if($(this).prop("value") == "step8")
        {
            window.location.replace('logout');
        }
    });
});