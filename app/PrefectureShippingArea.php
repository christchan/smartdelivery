<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class PrefectureShippingArea extends Model
{
    use SoftDeletes;
    use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'pref_id',
    ];

    public function prefectures()
    {
        return $this->hasOne('App\Prefecture','id','pref_id');
    }
}
