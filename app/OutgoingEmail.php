<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutgoingEmail extends Model
{
	use SoftDeletes;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'subject',
        'content',
        'destination_email',
        'destination_email_cc',
        'destination_email_bcc',
        'sent_at',
    ];
}
