<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Airport extends Model
{
    use SoftDeletes;
    use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'name',
    	'postcode',
    	'address',
    	'tel',
    	'latitude',
    	'longitude',
    	'lang_id',
    ];

    public function languages()
    {
        return $this->hasOne('App\Language','id','lang_id');
    }
}
