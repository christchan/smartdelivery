<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Language extends Model
{
   	use SoftDeletes;
   	use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'name',
    ];
}
