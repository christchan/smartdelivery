<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class ShippingCost extends Model
{
	use SoftDeletes;
	use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'weight',
    	'length',
    	'totalsize',
    	'packing_status',
    	'area_id',
    	'price',
    ];

    public function shippingarea()
    {
        return $this->belongsTo('App\ShippingArea','area_id','id');
    }
}
