<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Cartalyst\Stripe\Stripe;

use App;
use Session;
use Response;

use App\User;
use App\Shop;
use App\ShippingCost;
use App\InternationalShippingCost;
use App\PrefectureShippingArea;
use App\ContentItem;
use App\Prefecture;
use App\Airport;
use App\Country;
use App\Order;
use App\OverseasShipping;
use App\OverseasShippingContentItem;

class International extends Controller
{
   public function index($shop_id)
   {
      if (!Session::has('shop_id')) {
         return redirect('/');
      }
      else{
         if ($shop_id != Session::get('shop_id')) {
            return redirect('/');
         }
      }
      $data = array();
      $data['shop_id'] = $shop_id;
      $shop = Shop::where('id', $shop_id)->first();
      $shop_name = "";
      if ($shop) {
         $shop_name = $shop->name;
      }
      $data['shop_name'] = $shop_name;
      $data['prices'] = ShippingCost::whereIn('area_id',[1,2,3])->get();
      $price_table = array();
      for ($i=0; $i < count($data['prices']); $i++) { 
         $price = $data['prices'][$i];
         $weight = $price->weight;
         $totalsize = $price->totalsize;
         if (!array_key_exists($weight, $price_table)) {
            $price_table[$weight] = array();  
         }
         if (!array_key_exists($totalsize, $price_table[$weight])) {
            $price_table[$weight][$totalsize] = array();  
         }

         $price_table[$weight][$totalsize][] = $price;
      }

      $lang_id = 2;
      if (session('locale') == 'ja') {
         $lang_id = 1;
      }
      $data['price_table'] = $price_table;
      $data['content_items'] = ContentItem::where('lang_id',$lang_id)->get();
      $data['prefectures'] = Prefecture::where('lang_id', $lang_id)->get();
      $data['airports'] = Airport::where('lang_id', $lang_id)->get();
      $data['countries'] = Country::where('lang_id', $lang_id)->get();
      $data['today'] = date('Y-m-d H:i:s');

      $config = $this->stripeConfig();
      $data['stripe']['pk_key'] = $config['pk_key'];
      $data['stripe']['currency'] = $config['currency'];
      $data['stripe']['app_name'] = config('app.name');
      $data['stripe']['email'] = Auth::user()->email;
      $user_id = Auth::user()->id;
      $data['user'] = User::with(['shops'])->where('id', $user_id)->first();

      return view('international', $data);
   }

   public function getInternationalShippingCostReference(Request $request)
   {
      $response = array("status"=>"error", "message"=>"", "data"=>array());
      $area_id = $request->area_id;
      $prices = InternationalShippingCost::where('area_id', $area_id)->get();
      if (count($prices) > 0) {
         $response['status'] = 'success';
         $price_table = array();
         for ($i=0; $i < count($prices); $i++) { 
            $weight = $prices[$i]->weight;
            if (!array_key_exists($weight, $price_table)) {
               $price_table[$weight] = array();  
            }
            $price_table[$weight][] = $prices[$i];
         }
         $response['data'] = $price_table;
      }

      return $response;
   }

   public function getShippingCost(Request $request)
   {
      $response = array(
         "status" => "error",
         "message" => "",
         "data" => array(),
      );

      $shipping_information = $request->shipping_information;
      $packing_status = $shipping_information['packing_status'];
      $selected_continent = $shipping_information['selected_continent'];
      $size = $shipping_information['size'];
      $weight = $shipping_information['weight'];

      if (is_null($selected_continent) || is_null($size) || is_null($weight)) {
         return $response;
      }

      $cost = InternationalShippingCost::where('weight','>=',$weight)
               ->where('totalsize','>=',$size)
               ->where('area_id',$selected_continent)   
               ->where('packing_status',$packing_status)->first();
      if ($cost) {
         $response['status'] = 'success';
         $response['data'] = $cost;
      }

      return $response;
   }

   public function rule()
   {
         return view('int_step1_rule');
   }

   public function ems()
   {
         $data['prices'] = ShippingCost::all();
         return view('int_step2_ems', $data);
   }

   public function setItemSizeWeight()
   {
       return view('int_step3_size_int');
   }

   public function getItemSizeWeight(Request $request)
   {
         $status = $request->deliveryselection;
         $size = $request->size;
         $weight = $request->weight;

         Session::put(['status' => $status, 'size' => $size, 'weight' => $weight]);

         return redirect('/international_content_item');
   }

   public function setContentItems()
   {
      $lang_id = 2;
      if (session('locale') == 'ja') {
         $lang_id = 1;
      }
      //$data = $request->session()->all();
      $data['contents'] = ContentItem::where('lang_id',$lang_id)
                     ->get();
      return view('int_step4_content_items', $data);
   }

   public function getContentItems(Request $request)
   {
         $content_items = collect($request->content_items);
         $prices = collect($request->prices);
         $pieces = collect($request->pieces);
         $totals = collect($request->totals);

         $totalqty = 0;

         for($i=0;$i<$prices->count();$i++)
         {
            $totalqty += $request->pieces[$i];
         }
         
         $grandtotal = $request->grandtotal;

         Session::put(['content_items' => $content_items, 'prices' => $prices, 'pieces' => $pieces, 'totals' => $totals, 'totalqty' => $totalqty, 'grandtotal' => $grandtotal,]);

         return redirect('/international_destination_address');
   }

   public function setDestination()
   {
      $lang_id = 2;
      if (session('locale') == 'ja') {
         $lang_id = 1;
      }

      $data['countries'] = Country::where('lang_id', $lang_id);

      return view('int_step5_destination_address', $data);
   }

   public function getDestination(Request $request)
   {
      return redirect('/international_shipping');
   }
   /*
   public function calculateCost()
   {
      $size = Session::get('size');
      $weight = Session::get('weight');
      $status = Session::get('status');
      $deliveryselection = Session::get('deliveryselection');

      if($deliveryselection == "0")
      {
         $pref_area = PrefectureShippingArea::where('pref_id', Session::get('prefectureid'));
         if($pref_area->count() > 0)
            $area_id = 1;
         else
            $area_id = 3;
      }  
      elseif($deliveryselection == "1")
      {
         $area_id = 2;
      }
      
      $cost = ShippingCost::where('weight','>=',$weight)
               ->where('totalsize','>=',$size)
               ->where('area_id',$area_id)   
               ->where('packing_status',$status)->first();
      
      return $cost->price;
   }
   */
   public function setPaymentMethod()
   {
      $config = $this->stripeConfig();

      $data['pk_key'] = $config['pk_key'];
      $data['currency'] = $config['currency'];

      return view('int_step6_shipping', $data);
   }

   public function stripeConfig()
   {
      $stripe_config = Config::get('services.stripe');
       
       if ($stripe_config['mode'] == 'test') {
           $pk_key = $stripe_config['pk_test'];
           $sk_key = $stripe_config['sk_test'];
       } else {
           $pk_key = $stripe_config['pk_live'];
           $sk_key = $stripe_config['sk_live'];
       }

       $config = [
            'pk_key' => $pk_key,
            'sk_key' => $sk_key,
            'currency' => $stripe_config['currency'],
       ];

       return $config;
   }

   public function getPaymentMethod(Request $request)
   {
      $payment = $request->payment;

      Session::put('payment_method', $payment);

      if($payment == "Credit Card")
      {
         //return redirect('/creditcard_payment');
         $this->getCreditCardInfo($request);
         return redirect('/payment_complete');
      }

      elseif($payment == "Wechat")
         return redirect('/wechat_payment');
      
      elseif($payment == "Alipay")
         return redirect('/alipay_payment');

      elseif($payment == "Unionpay")
         return redirect('/unionpay_payment');
      
      else
         return redirect('/creditcard_payment');
   }
   /*
   public function setCreditCardInfo()
   {
      return view('dom_step7_creditcard');
   }
   */
   public function getCreditCardInfo(Request $request)
   {
      $config = $this->stripeConfig();

      // Set your secret key: remember to change this to your live secret key in production
      // See your keys here: https://dashboard.stripe.com/account/apikeys
      $stripe = Stripe::make($config['sk_key'], '2018-02-06');

      // Token is created using Checkout or Elements!
      // Get the payment token ID submitted by the form:
      $token = $request->stripeToken;
      
      // Charge the user's card:
      $charge = $stripe->charges()->create([
        "amount" => Session::get('cost'),
        "currency" => $config['currency'],
        "description" => "Payment",
        "source" => $token,
      ]);
   }

   public function setWechatPayment()
   {
      return view('dom_step7_wechat');
   }

   public function setAlipayPayment()
   {
      return view('dom_step7_alipay');
   }

   public function setUnionpayPayment()
   {
      return view('dom_step7_unionpay');
   }

   public function payment_complete()
   {
      //$data = $request->session()->all();

      return view('dom_step8_payment_complete');
   }

   public function downloadCSV(Request $request)
   {
      $response = array("status"=>"error", "message"=>"", "data"=>array());
      $input = $request->all();
      $from = $input['from'];
      $to = $input['to'];
      if ($from != '' && $to != '') {
         $data = OverseasShipping::with(['user','shop','contents' => function($query){
            $query->with(['contentitem']);
         }])->where('created_at', '>=', $from." 00:00:00")->where('created_at', '<=', $to." 23:59:59")->get();
         $filename = "overseas_shipping_report_".date('Ymd', strtotime($from))."-".date('Ymd', strtotime($to)).".csv";
      }
      else {
         $data = OverseasShipping::with(['user','shop','contents' => function($query){
            $query->with(['contentitem']);
         }])->get();
         $filename = "overseas_shipping_report_all.csv";
      }

      $csv_path = storage_path()."/csv/";
      $csv_fullpath = $csv_path.$filename;
      if (!file_exists($csv_path)) {
         mkdir($csv_path, 0777, true);
      }

      $max_count_item = 0;
      for ($i=0; $i < count($data); $i++) { 
         $temp_count_item = count($data[$i]->contents);
         if ($temp_count_item > $max_count_item) {
            $max_count_item = $temp_count_item;
         }
      }

      // $headers = array(
      //    "Owner Name", "Shop Name", "Record ID", "Payment Type",
      //    "Payment Method", "Total Item Qty.", "Grand Total Items", "Packing Status",
      //    "Weight", "Size", "Price", "Cool Type",
      //    "Slip ID", "Shipping Date", "Delivery Date", "Delivery Time Range",
      //    "Delivery Code", "Customer Tel", "Customer Tel Edaban", "Customer Post Code",
      //    "Customer Address 1", "Customer Address 2", "Company Name", "Company Division", 
      //    "Customer Name", "Customer Name Kana", "Customer Email", "Customer Sir",
      //    "Agent Code", "Agent Tel", "Agent Tel Edaban", "Agent Post Code", 
      //    "Agent Address 1", "Agent Address 2", "Agent Name", "Agent Name Kana",
      //    "Remarks", "Collect Payment Amount", "Collect Payment Tax", "Eigyo Stop",
      //    "Eigyo Code", "Hakko Maisu", "Kosu Flag", "Item 1 Atsukai", "Item 2 Atsukai",
      // );
      // for ($i=0; $i < $max_count_item; $i++) { 
      //    $num = $i+1;
      //    array_push($headers, "Item ".$num." Code");
      //    array_push($headers, "Item ".$num." Name");
      //    array_push($headers, "Item ".$num." Qty");
      //    array_push($headers, "Item ".$num." Unit Price");
      // }
      // for ($i=1; $i <= 34; $i++) { 
      //    array_push($headers, "Others ".$i);
      // }
      // $rows = array();
      // for ($i=0; $i < count($data); $i++) {
      //    $d = $data[$i];
      //    $temp_row = array(
      //       $d->user->name, $d->shop->name, $d->record_id, $d->payment_type,
      //       $d->payment_method, $d->item_total_qty, $d->item_grand_total, $d->packing_status,
      //       $d->total_weight, $d->total_size, $d->total_price, $d->cool_type,
      //       $d->slip_id, $d->shipping_date, $d->delivery_date, $d->delivery_time_range,
      //       $d->delivery_code, $d->customer_tel, $d->customer_tel_edaban, $d->customer_post_code,
      //       $d->customer_address1, $d->customer_address2, $d->company_name, $d->company_division,
      //       $d->customer_name, $d->customer_name_kana, $d->customer_email, $d->customer_sir,
      //       $d->agent_code, $d->agent_tel, $d->agent_tel_edaban, $d->agent_post_code,
      //       $d->agent_address1, $d->agent_address2, $d->agent_name, $d->agent_name_kana,
      //       $d->remarks, $d->collect_payment_amount, $d->collect_payment_tax, $d->eigyo_stop,
      //       $d->eigyo_code, $d->hakko_maisu, $d->kosu_flag, $d->item1_atsukai, $d->item2_atsukai
      //    );

      //    for ($j=0; $j < count($d->contents); $j++) { 
      //       $c = $d->contents[$j];
      //       array_push($temp_row, "");
      //       array_push($temp_row, $c->contentitem->name);
      //       array_push($temp_row, $c->piece);
      //       array_push($temp_row, $c->price);
      //    }

      //    for ($j=1; $j <= 34; $j++) { 
      //       array_push($temp_row, "");
      //    }

      //    array_push($rows, $temp_row);
      // }


      $headers = array(
         "Record ID", "Shipping Way Code", "Shipping Company", "Customer Name",
         "Customer Address 1", "Customer Address 2", "Customer Email", "Customer City",
         "Customer State", "Customer Post Code", "Customer Tel", "Customer Country",
         "Remarks", "Shipping Company Code", "Product Type", "Total Price", "Total Weight"
      );
      for ($i=0; $i < $max_count_item; $i++) { 
         $num = $i+1;
         array_push($headers, "Item ".$num." Code");
         array_push($headers, "Item ".$num." Name");
         array_push($headers, "Item ".$num." HS Code");
         array_push($headers, "Item ".$num." Made Country Code");
         array_push($headers, "Item ".$num." Pieces");
         array_push($headers, "Item ".$num." Unit Price");
         array_push($headers, "Item ".$num." Weight");
      }
      $rows = array();
      for ($i=0; $i < count($data); $i++) {
         $d = $data[$i];
         $temp_row = array(
            $d->record_id, $d->shipping_way_code, $d->shipping_company, $d->customer_name,
            $d->customer_other_address, $d->customer_other_address2, $d->customer_email, $d->customer_city,
            $d->customer_state, $d->customer_post_code, $d->customer_tel, $d->customer_country,
            $d->remarks, $d->shipping_company_code, $d->product_type, $d->total_price, $d->total_weight
         );
         for ($j=0; $j < $max_count_item; $j++) { 
            if (isset($d->contents[$j])) {
               $c = $d->contents[$j];
               array_push($temp_row, "");
               array_push($temp_row, $c->contentitem->name);
               array_push($temp_row, "");
               array_push($temp_row, "");
               array_push($temp_row, $c->piece);
               array_push($temp_row, $c->price);
               array_push($temp_row, "");
            }
            else {
               array_push($temp_row, "");
               array_push($temp_row, "");
               array_push($temp_row, "");
               array_push($temp_row, "");
               array_push($temp_row, "");
               array_push($temp_row, "");
               array_push($temp_row, "");
            }
         }

         array_push($rows, $temp_row);
      }

      $file = fopen($csv_fullpath, 'w');
      fputcsv($file, $headers);
      for ($i=0; $i < count($rows); $i++) { 
         fputcsv($file, $rows[$i]);
      }
      fclose($file);

      $response["status"] = "success";
      $response["data"] = $filename;
      return $response;
   }
}
