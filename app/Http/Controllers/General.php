<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;
use Cartalyst\Stripe\Stripe;

use Carbon\Carbon;
use App\User;
use App\Shop;
use App\ShippingCost;
use App\PrefectureShippingArea;
use App\ContentItem;
use App\Prefecture;
use App\Airport;
use App\Order;
use App\DomesticShipping;
use App\DomesticShippingContentItem;
use App\OverseasShipping;
use App\OverseasShippingContentItem;
use App\OutgoingEmail;
use App\Page;
use Session;

use App\Http\Controllers\Domestic as DomesticController;
use App\Http\Controllers\EmailController as EmailController;

class General extends Controller
{
    public function login()
    {
        return view('loginpage');
    }

    public function loginprocess(Request $request)
    {
        if(Auth::attempt($request->only('email', 'password')))
        {
            return response(array(
                     'response' => true,
                     ),200);    
        }
        else
        {
            return response(array(
                 'response' => false,
                 ),200);
        }
    }

    public function indexHomepage()
    {
        if (Session::has('shop_id')) {
            $data = array();
            $data['shop_id'] = Session::get('shop_id');
            $data['shop_name'] = Shop::where('id', $data['shop_id'])->first()->name;
            return view('homepage', $data);
        }
        else {
            return redirect('/selectshop');
        }
    }

    public function saveOrder(Request $request)
    {
        $response = array(
            "status" => "error",
            "message" => "",
            "data" => array(),
        );
        $shipping_id = 0;
        $data = $request->data;
        $token = $data['token'];
        $shipping_information = $data['shipping_information'];

        $user_id = Auth::user()->id;
        $shop_id = $shipping_information['shop_id'];
        $user = User::with(['shops'])->where('id', $user_id)->first();

        $DC = new DomesticController();
        $config = $DC->stripeConfig();
        $stripe = Stripe::make($config['sk_key'], '2018-02-06');
        $charge = $stripe->charges()->create([
            "amount" => $shipping_information['shipping_cost'],
            "currency" => $config['currency'],
            "description" => "Smart Delivery Payment",
            "receipt_email" => $user->email,
            "source" => $token['id'],
        ]);
        $stripe_charge_id = $charge['id'];

        $contents = $this->generateItemContentNameString($shipping_information['items']);
        $shop_name = "";
        if (count($user->shops) > 0) {
            $shop_name = $user->shops[0]->name;
        }
        $order_date = date("Y-m-d H:i:s");
        $shipping_date = date('Y-m-d 00:00:00', strtotime($order_date . ' +1 day'));

        $shipping_type = $shipping_information['shipping_type'];
        $destination_type = $shipping_information['destination_type'];
        $destination_address = "";
        $destination_tel = "";
        $company_name = "";
        if ($shipping_type == 'domestic') {
            if ($destination_type == 'address') {
                $destination_address = $shipping_information['destination_info']['address'] . ", " . $shipping_information['destination_info']['prefecture_name'] . ", " . $shipping_information['destination_info']['post_code'];
                $company_name = $shipping_information['destination_info']['company_name'];
            }
            else {
                $destination_address = $shipping_information['destination_info']['airport_name'] . ", " . $shipping_information['destination_info']['airport_address'] . ", " . $shipping_information['destination_info']['airport_tel'] . ", " . $shipping_information['destination_info']['airport_postcode'];
            }
        }
        else {
            $destination_tel = $shipping_information['destination_info']['tel'];
            $destination_address = $shipping_information['destination_info']['address'] . ", " . $shipping_information['destination_info']['prefecture_name'] . ", " . $shipping_information['destination_info']['country_name'] . ", " . $shipping_information['destination_info']['post_code'];
            $customer_name = $shipping_information['destination_info']['name'];
            $customer_tel = $shipping_information['destination_info']['tel'];
        }

        if ($shipping_type == 'domestic') {
            $delivery_date = $shipping_information['destination_info']['delivery_date'];
            $data_shipping = array(
                "user_id" => $user_id,
                "shop_id" => $shop_id,
                "record_id" => "",
                "stripe_charge_id" => $stripe_charge_id,
                "payment_method" => $shipping_information['payment_method'],
                "item_total_qty" => $shipping_information['items_total_qty'],
                "item_grand_total" => $shipping_information['items_grand_total'],
                "packing_status" => $shipping_information['packing_status'],
                "total_weight" => $shipping_information['weight'],
                "total_size" => $shipping_information['size'],
                "total_price" => $shipping_information['shipping_cost'],
                "shipping_date" => $shipping_date,
                "delivery_date" => $delivery_date,
                "customer_tel" => $shipping_information['destination_info']['tel'],
                "customer_name" => $shipping_information['destination_info']['name'],
                "customer_email" => $shipping_information['destination_info']['email'],
                "customer_address1" => $destination_address,
                "customer_address2" => "",
                "company_name" => $company_name,
                "agent_address2" => "",
                "item1_name" => $contents,
                "remarks" => "",
                "receipt_id" => "",
            );
            if ($destination_type == "address") {
                $data_shipping['customer_post_code'] = $shipping_information['destination_info']['post_code'];
            }
            else {
                $data_shipping['customer_post_code'] = $shipping_information['destination_info']['airport_postcode'];
            }
            $shipping = DomesticShipping::create($data_shipping);
            $shipping_id = $shipping->id;
            $code = $this->generateShippingCode($shipping_type, $shipping);
            $tempdate = str_replace("-", "", $shipping->created_at);
            $tempdate = str_replace(":", "", $tempdate);
            $tempdate = str_replace(" ", "", $tempdate);
            $receipt_id = "D".$tempdate."-M01-001";
            DomesticShipping::where('id', $shipping->id)->update(['record_id' => $code]);

            if($shipping_information['items'] != ''){
                for ($i=0; $i < count($shipping_information['items']); $i++) { 
                    $data_items['shipping_id'] = $shipping->id;
                    $data_items['content_item'] = $shipping_information['items'][$i]['content_item'];
                    $data_items['price'] = $shipping_information['items'][$i]['price'];
                    $data_items['piece'] = $shipping_information['items'][$i]['piece'];

                    DomesticShippingContentItem::create($data_items);
                }
            }
        }
        else {
            $delivery_date = $shipping_information['destination_info']['delivery_date'];
            $data_shipping = array(
                "user_id" => $user_id,
                "shop_id" => $shop_id,
                "record_id" => "",
                "stripe_charge_id" => $stripe_charge_id,
                "shipping_way_code" => "111",
                "payment_method" => $shipping_information['payment_method'],
                "item_total_qty" => $shipping_information['items_total_qty'],
                "item_grand_total" => $shipping_information['items_grand_total'],
                "packing_status" => $shipping_information['packing_status'],
                "shipping_date" => $shipping_date,
                "delivery_date" => $delivery_date,
                "customer_name" => $customer_name,
                "customer_email" => $shipping_information['destination_info']['email'],
                "customer_other_address" => $shipping_information['destination_info']['address'],
                "customer_other_address2" => "",
                "customer_city" => $shipping_information['destination_info']['city'],
                "customer_state" => $shipping_information['destination_info']['prefecture_name'],
                "customer_post_code" => $shipping_information['destination_info']['post_code'],
                "customer_tel" => $customer_tel,
                "customer_country" => $shipping_information['destination_info']['country_name'],
                "selected_continent" => $shipping_information['selected_continent'],
                "remarks" => "",
                "total_price" => $shipping_information['shipping_cost'],
                "total_weight" => $shipping_information['weight'],
                "total_size" => $shipping_information['size'],
                "item1_name" => $contents,
            );
            $shipping = OverseasShipping::create($data_shipping);
            $shipping_id = $shipping->id;
            $code = $this->generateShippingCode($shipping_type, $shipping);
            $tempdate = str_replace("-", "", $shipping->created_at);
            $tempdate = str_replace(":", "", $tempdate);
            $tempdate = str_replace(" ", "", $tempdate);
            $remarks = "O".$tempdate."-M01-001";
            OverseasShipping::where('id', $shipping->id)->update(['record_id' => $code]);

            if($shipping_information['items'] != ''){
                for ($i=0; $i < count($shipping_information['items']); $i++) { 
                    $data_items['shipping_id'] = $shipping->id;
                    $data_items['content_item'] = $shipping_information['items'][$i]['content_item'];
                    $data_items['price'] = $shipping_information['items'][$i]['price'];
                    $data_items['piece'] = $shipping_information['items'][$i]['piece'];

                    OverseasShippingContentItem::create($data_items);
                }
            }
        }

        $EC = new EmailController();
        $admin_mail_content = $EC->generateMailContentForAdmin($shipping_id, $shipping_type);
        $owner_mail_content = $EC->generateMailContentForOwner($shipping_id, $shipping_type);
        $customer_mail_content = $EC->generateMailContentForCustomer($shipping_id, $shipping_type);
        // $admin = User::find(1);
        // $admin_email = "";
        // if ($admin) {
        //     $admin_email = $admin->email;
        // }
        $admin_email = env('ADMIN_EMAIL');
        $admin_email2 = env('ADMIN_EMAIL2');
        $data_email_admin = array(
            "subject" => '配送注文の支払が完了しました',
            "content" => $admin_mail_content,
            "destination_email" => $admin_email,
            "destination_email_cc" => $admin_email2,
            "destination_email_bcc" => "",
        );
        $data_email_owner = array(
            "subject" => '配送注文の支払が完了しました',
            "content" => $owner_mail_content,
            "destination_email" => $user->email,
            "destination_email_cc" => "",
            "destination_email_bcc" => "",
        );
        $data_email_customer = array(
            "subject" => '配送注文の支払が完了しました',
            "content" => $customer_mail_content,
            "destination_email" => $shipping_information['destination_info']['email'],
            "destination_email_cc" => "",
            "destination_email_bcc" => "",
        );
        OutgoingEmail::create($data_email_admin);
        OutgoingEmail::create($data_email_owner);
        OutgoingEmail::create($data_email_customer);

        $EC->sentMails();

        $response['status'] = 'success';
        $response['data'] = $code;

        return $response;
    }

    public function generateShippingCode($shipping_type, $order)
    {
        $now = date('Ymd');
        $code = "";
        if ($shipping_type == 'domestic') {
            $code = 'D';
        }
        else {
            $code = 'O';
        }
        $code .= $now."-";
        $code_num = str_pad($order->id, 3, '0', STR_PAD_LEFT);
        $code .= $code_num;
        return $code;
    }

    public function generateItemContentNameString($items)
    {
        $lang_id = 2;
        if (session('locale') == 'ja') {
            $lang_id = 1;
        }
        $contentitems = ContentItem::where('lang_id', $lang_id)->get();
        $string = "";
        if (count($contentitems) > 0) {
            for ($i=0; $i < count($items); $i++) { 
                $item = $items[$i];
                $content = $contentitems->where('id', $item['content_item']);
                if (count($content) > 0) {
                    if ($string == "") {
                        $string = $content->first()->name;
                    }
                    else {
                        $string .= ", ".$content->first()->name;
                    }
                }
            }
        }
        
        return $string;
    }

    public function indexPrintSlip($shipping_code)
    {
        $data = array();
        $order = Order::where('shipping_code', $shipping_code)->first();
        $data['order'] = $order;

        return view('print_slip', $data);
    }

    public function indexProfile()
    {
        $data = array();
        $data['user'] = Auth::user();
        return view('profile', $data);
    }

    public function editProfile(Request $request)
    {
        $response = array(
            "status" => "error",
            "message" => "",
            "data" => array(),
        );

        $user = Auth::user();
        $type = $request->type;
        $data = $request->data;
        if ($type == "basic") {
            $name = $data['name'];
            User::where('id', $user->id)->update(['name' => $name]);
            $response['status'] = 'success';
            $response['message'] = __('lang.update_success');
        }
        else {
            $password = $data['password'];
            User::where('id', $user->id)->update(['password' => bcrypt($password)]);
            $response['status'] = 'success';
            $response['message'] = __('lang.update_success');
        }

        return $response;
    }

    public function myShipments()
    {
        $data = array();
        $user_id = Auth::user()->id;
        $data['overseas'] = OverseasShipping::where('user_id', $user_id)->get();
        $data['domestic'] = DomesticShipping::where('user_id', $user_id)->get();
        $data['myshipment'] = array();
        $overseas_count = 0;

        for ($i=0; $i < count($data['overseas']); $i++) { 
            $shop = Shop::where('id',$data['overseas'][$i]->shop_id)->first();

            $data['myshipment'][$i]['id']           = $data['overseas'][$i]->id;
            $data['myshipment'][$i]['date']         = $data['overseas'][$i]->created_at->toDateString();;
            $data['myshipment'][$i]['type']         = 'overseas';
            $data['myshipment'][$i]['record_id']    = $data['overseas'][$i]->record_id;
            $data['myshipment'][$i]['shop_name']    = $shop->name;
            $data['myshipment'][$i]['items']        = array();

            $oitems = OverseasShippingContentItem::join('content_items','content_items.id','=','overseas_shipping_content_items.content_item')->where('shipping_id',$data['overseas'][$i]->id)->get();

            if (!empty($oitems)) {
                for ($oi=0; $oi < count($oitems); $oi++) { 
                    $data['myshipment'][$i]['items'][$oi]['name'] = $oitems[$oi]->name;
                    $data['myshipment'][$i]['items'][$oi]['price'] = $oitems[$oi]->price;
                    $data['myshipment'][$i]['items'][$oi]['piece'] = $oitems[$oi]->piece;
                }
            }

            $data['myshipment'][$i]['destination_name']             = $data['overseas'][$i]->customer_name;
            $data['myshipment'][$i]['destination_address']          = $data['overseas'][$i]->customer_other_address;
            $data['myshipment'][$i]['destination_city']             = $data['overseas'][$i]->customer_city;
            $data['myshipment'][$i]['destination_state']            = $data['overseas'][$i]->customer_state;
            $data['myshipment'][$i]['destination_post_code']        = $data['overseas'][$i]->customer_post_code;
            $data['myshipment'][$i]['destination_customer_tel']     = $data['overseas'][$i]->customer_tel;
            $data['myshipment'][$i]['destination_customer_country'] = $data['overseas'][$i]->customer_country;
            $data['myshipment'][$i]['payment_method']               = $data['overseas'][$i]->payment_method;
            $data['myshipment'][$i]['item_grand_total']             = $data['overseas'][$i]->item_grand_total;
            $data['myshipment'][$i]['packing_status']               = $data['overseas'][$i]->packing_status;
            $data['myshipment'][$i]['total_weight']                 = $data['overseas'][$i]->total_weight;
            $data['myshipment'][$i]['total_size']                   = $data['overseas'][$i]->total_size;
            $data['myshipment'][$i]['total_price']                  = $data['overseas'][$i]->total_price;
        }

        $overseas_count = count($data['overseas']);

        for ($j=0; $j < count($data['domestic']); $j++) { 
            $domestic_index = $overseas_count + $j;
            $shop = Shop::where('id',$data['domestic'][$j]->shop_id)->first();

            $data['myshipment'][$domestic_index]['id']           = $data['domestic'][$j]->id;
            $data['myshipment'][$domestic_index]['date']         = $data['domestic'][$j]->created_at->toDateString();;
            $data['myshipment'][$domestic_index]['type']         = 'domestic';
            $data['myshipment'][$domestic_index]['record_id']    = $data['domestic'][$j]->record_id;
            $data['myshipment'][$domestic_index]['shop_name']    = $shop->name;
            $data['myshipment'][$domestic_index]['items']        = array();

            $ditems = DomesticShippingContentItem::join('content_items','content_items.id','=','domestic_shipping_content_items.content_item')->where('shipping_id',$data['domestic'][$j]->id)->get();

            if (!empty($ditems)) {
                for ($di=0; $di < count($ditems); $di++) { 
                    $data['myshipment'][$domestic_index]['items'][$di]['name'] = $ditems[$di]->name;
                    $data['myshipment'][$domestic_index]['items'][$di]['price'] = $ditems[$di]->price;
                    $data['myshipment'][$domestic_index]['items'][$di]['piece'] = $ditems[$di]->piece;
                }
            }

            $data['myshipment'][$domestic_index]['destination_name']             = $data['domestic'][$j]->customer_name;
            $data['myshipment'][$domestic_index]['destination_address']          = $data['domestic'][$j]->customer_address1;
            $data['myshipment'][$domestic_index]['destination_city']             = $data['domestic'][$j]->customer_city;
            $data['myshipment'][$domestic_index]['destination_state']            = $data['domestic'][$j]->customer_state;
            if ($data['domestic'][$j]->customer_post_code=='') {
                $post_code = $data['domestic'][$j]->agent_post_code;
            }else{
                $post_code = $data['domestic'][$j]->customer_post_code;
            }

            $data['myshipment'][$domestic_index]['destination_post_code']        = $post_code;

            if ($data['domestic'][$j]->customer_tel) {
                $tel = $data['domestic'][$j]->agent_tel;
            }else{
                $tel = $data['domestic'][$j]->customer_tel;
            }

            $data['myshipment'][$domestic_index]['destination_customer_tel']     = $tel;
            $data['myshipment'][$domestic_index]['destination_customer_country'] = 'Japan';
            $data['myshipment'][$domestic_index]['delivery_date'] = $data['domestic'][$j]->delivery_date;
            $data['myshipment'][$domestic_index]['payment_method']               = $data['domestic'][$j]->payment_method;
            $data['myshipment'][$domestic_index]['item_grand_total']             = $data['domestic'][$j]->item_grand_total;
            $data['myshipment'][$domestic_index]['packing_status']               = $data['domestic'][$j]->packing_status;
            $data['myshipment'][$domestic_index]['total_weight']                 = $data['domestic'][$j]->total_weight;
            $data['myshipment'][$domestic_index]['total_size']                   = $data['domestic'][$j]->total_size;
            $data['myshipment'][$domestic_index]['total_price']                  = $data['domestic'][$j]->total_price;
            
        }

        return response()->json($data['myshipment']);

    }

    public function deleteShipment($type, $id)
    {
        if ($type=='overseas') {
            OverseasShipping::where('id',$id)->delete();
        }else{
            DomesticShipping::where('id',$id)->delete();
        }

        return response()->json(['response' =>true]);
    }

    public function getPageContent($slug)
    {
        $data = array();
        $data['page'] = Page::where('slug',$slug)->first();

        return view('custom_pages', $data);
    }

    public function checkPassword(Request $request)
    {
        $response = array(
            "status" => "error",
            "message" => "",
            "data" => array(),
        );
        $hasher = app('hash');

        $input = $request->all();
        $user = Auth::user();
        if ($hasher->check($input['password'], $user->password)) {
            $response["status"] = "success";
        }
        else {
            $response["message"] = __('lang.password_not_authorized');
        }

        return $response;
    }

    public function indexDownloadCsv($filename)
    {
        $fullpath = storage_path('csv/'.$filename);
        return response()->download($fullpath);
    }
}
