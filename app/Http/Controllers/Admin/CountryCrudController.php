<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\CountryCrudRequest as StoreRequest;
use App\Http\Requests\CountryCrudRequest as UpdateRequest;

class CountryCrudController extends CrudController {

	public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\Country");
        $this->crud->setRoute("admin/country");
        $this->crud->setEntityNameStrings('Country', 'country');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => "Name",
        ]);

         $this->crud->addColumn([
            'name'  => 'lang_id',
            'label' => "Language",
            'type' => "select",
            'entity' => 'languages', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Language", // foreign key model
        ]);
      

        // ------ CRUD FIELDS
        $this->crud->addField([ 
            'name'  => 'name',
            'label' => 'Name',
            'type'  => 'text',
        ]);

        $this->crud->addField([
          'name'  => 'lang_id',
          'label' => "Select Language",
          'type' => 'select',
          'entity' => 'languages',
          'attribute' => "name",
          'model' => "App\Language"
        ]);


    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }


}