<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\ShippingCostCrudRequest as StoreRequest;
use App\Http\Requests\ShippingCostCrudRequest as UpdateRequest;
use App\Page;

class ShippingCostCrudController extends CrudController {

	public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\ShippingCost");
        $this->crud->setRoute("admin/shipping_cost");
        $this->crud->setEntityNameStrings('Shipping Cost', 'shipping cost');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name'  => 'weight',
            'label' => "Weight",
        ]);

        $this->crud->addColumn([
            'name'  => 'length',
            'label' => "Length",
        ]);

        $this->crud->addColumn([
            'name'  => 'totalsize',
            'label' => "Total Size",
        ]);

        $this->crud->addColumn([
            'name'  => 'totalsize',
            'label' => "Total Size",
        ]);

        $this->crud->addColumn([
            'name'  => 'packing_status',
            'label' => "Packing Status",
        ]);

        $this->crud->addColumn([
            'name'  => 'packing_status',
            'label' => "Packing Status",
        ]);

        $this->crud->addColumn([
            'name'  => 'area_id',
            'label' => "Area",
            'type' => "select",
            'entity' => 'shippingarea', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\ShippingArea", // foreign key model
        ]);

        $this->crud->addColumn([
            'name'  => 'price',
            'label' => "Price",
        ]);
      

        // ------ CRUD FIELDS
        $this->crud->addField([ 
            'name'  => 'weight',
            'label' => 'Weight',
            'type'  => 'number',
        ]);

        $this->crud->addField([ 
            'name'  => 'length',
            'label' => 'Length',
            'type'  => 'number',
        ]);

        $this->crud->addField([ 
            'name'  => 'totalsize',
            'label' => 'Total Size',
            'type'  => 'number',
        ]);

        $this->crud->addField([
           'name' => 'packing_status',
           'label' => "packing Status",
           'type'      => 'radio',
           'options'   => [ '1' => 'yes',
                            '0' => 'no'

           ],
       ]);

        $this->crud->addField([
            'name'  => 'area_id',
            'label' => "Area",
            'type' => "select",
            'entity' => 'shippingarea', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\ShippingArea", // foreign key model
        ]);

        $this->crud->addField([ 
            'name'  => 'price',
            'label' => 'Price',
            'type'  => 'number',
        ]);


    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }


}