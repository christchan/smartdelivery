<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\AirportCrudRequest as StoreRequest;
use App\Http\Requests\AirportCrudRequest as UpdateRequest;

class AirportCrudController extends CrudController {

	public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\Airport");
        $this->crud->setRoute("admin/airport");
        $this->crud->setEntityNameStrings('Airport', 'airport');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => "Name",
        ]);

        $this->crud->addColumn([
            'name'  => 'postcode',
            'label' => "Postcode",
        ]);

        $this->crud->addColumn([
            'name'  => 'address',
            'label' => "Address",
        ]);

        $this->crud->addColumn([
            'name'  => 'tel',
            'label' => "Tel",
        ]);

        $this->crud->addColumn([
            'name'  => 'latitude',
            'label' => "Latitude",
        ]);

        $this->crud->addColumn([
            'name'  => 'longitude',
            'label' => "Longitude",
        ]);

        $this->crud->addColumn([
            'name'  => 'lang_id',
            'label' => "Language",
            'type' => "select",
            'entity' => 'languages', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Language", // foreign key model
        ]);
      

        // ------ CRUD FIELDS
        $this->crud->addField([ 
            'name'  => 'name',
            'label' => 'Name',
            'type'  => 'text',
        ]);

        $this->crud->addField([ 
            'name'  => 'postcode',
            'label' => 'Postcode',
            'type'  => 'text',
        ]);

        $this->crud->addField([ 
            'name'  => 'address',
            'label' => 'Address',
            'type'  => 'text',
        ]);

        $this->crud->addField([ 
            'name'  => 'tel',
            'label' => 'Tel',
            'type'  => 'text',
        ]);

        $this->crud->addField([ 
            'name'  => 'latitude',
            'label' => 'latitude',
            'type'  => 'number',
        ]);

        $this->crud->addField([ 
            'name'  => 'longitude',
            'label' => 'longitude',
            'type'  => 'number',
        ]);

        $this->crud->addField([
          'name'  => 'lang_id',
          'label' => "Select Language",
          'type' => 'select',
          'entity' => 'languages',
          'attribute' => "name",
          'model' => "App\Language"
        ]);

    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        print_r('a');
        // print_r($request->all());die();
        // return parent::updateCrud();
    }


}