<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\PrefectureShippingAreaCrudRequest as StoreRequest;
use App\Http\Requests\PrefectureShippingAreaCrudRequest as UpdateRequest;
use App\Page;

class PrefectureShippingAreaCrudController extends CrudController {

    public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\PrefectureShippingArea");
        $this->crud->setRoute("admin/prefecture_shipping_area");
        $this->crud->setEntityNameStrings('Prefecture Shipping Area', 'prefecture shipping area');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS

         $this->crud->addColumn([
            'name'  => 'pref_id',
            'label' => "Prefecture",
            'type' => "select",
            'entity' => 'prefectures', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Prefecture", // foreign key model
        ]);
      

        // ------ CRUD FIELDS

        $this->crud->addField([
          'name'  => 'pref_id',
            'label' => "Prefecture",
            'type' => "select",
            'entity' => 'prefectures', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Prefecture", // foreign key model
        ]);


    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }


}