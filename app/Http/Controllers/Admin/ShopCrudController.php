<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\ShopCrudRequest as StoreRequest;
use App\Http\Requests\ShopCrudRequest as UpdateRequest;
use App\Page;

class ShopCrudController extends CrudController {

	public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\Shop");
        $this->crud->setRoute("admin/shop");
        $this->crud->setEntityNameStrings('Shop', 'shop');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => "Name",
        ]);

         $this->crud->addColumn([
            'name'  => 'user_id',
            'label' => "User",
            'type' => "select",
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\User", // foreign key model
        ]);
      

        // ------ CRUD FIELDS
        $this->crud->addField([ 
            'name'  => 'name',
            'label' => 'Name',
            'type'  => 'text',
        ]);

        $this->crud->addField([
            'name'  => 'user_id',
            'label' => "User",
            'type' => "select",
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\User", // foreign key model
        ]);


    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }


}