<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\OverseasShippingCrudRequest as StoreRequest;
use App\Http\Requests\OverseasShippingCrudRequest as UpdateRequest;

class OverseasShippingCrudController extends CrudController {

	public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\OverseasShipping");
        $this->crud->setRoute("admin/overseas_shipping_cost");
        $this->crud->setEntityNameStrings('Overseas Shipping Cost', 'overseas shipping');
        $this->crud->removeAllButtons();
        $this->crud->addButtonFromView('top', 'date_range_filter', 'overseasshippingdatefilter', 'beginning');

        if (isset($_GET['from']) && isset($_GET['to'])) {
            $from = $_GET['from'];
            $to = $_GET['to'];

            $this->crud->addClause('where', 'created_at', '>=', $from." 00:00:00");
            $this->crud->addClause('where', 'created_at', '<=', $to." 23:59:59");
        }

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name'  => 'user_id',
            'label' => "User",
            'type' => "select",
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\User", // foreign key model
        ]);

        $this->crud->addColumn([
            'name'  => 'shop_id',
            'label' => "Shop",
            'type' => "select",
            'entity' => 'shop', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Shop", // foreign key model
        ]);

        $this->crud->addColumn([
            'name'  => 'record_id',
            'label' => "Record ID",
        ]);

        $this->crud->addColumn([
            'name'  => 'stripe_charge_id',
            'label' => "Stripe Charge ID",
        ]);

        $this->crud->addColumn([
            'name'  => 'shipping_way_code',
            'label' => "Shipping Way Code",
        ]);

        $this->crud->addColumn([
            'name'  => 'customer_name',
            'label' => "Customer Name",
        ]);

        $this->crud->addColumn([
            'name'  => 'customer_tel',
            'label' => "Customer Tel",
        ]);

        $this->crud->addColumn([
            'name'  => 'customer_other_address',
            'label' => "Customer Address",
        ]);

        $this->crud->addColumn([
            'name'  => 'customer_city',
            'label' => "Customer City",
        ]);

        $this->crud->addColumn([
            'name'  => 'customer_state',
            'label' => "Customer State",
        ]);

        $this->crud->addColumn([
            'name'  => 'customer_post_code',
            'label' => "Customer Post Code",
        ]);

        $this->crud->addColumn([
            'name'  => 'customer_country',
            'label' => "Customer Country",
        ]);

        $this->crud->addColumn([
            'name'  => 'total_price',
            'label' => "Price",
        ]);

        $this->crud->addColumn([
            'name'  => 'item1_name',
            'label' => "Items",
        ]);

        $this->crud->addColumn([
            'name'  => 'created_at',
            'label' => "Created At",
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}