<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\LanguageCrudRequest as StoreRequest;
use App\Http\Requests\languageCrudRequest as UpdateRequest;
use App\Page;

class LanguageCrudController extends CrudController {

	public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\Language");
        $this->crud->setRoute("admin/language");
        $this->crud->setEntityNameStrings('Language', 'language');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => "Name",
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([ 
            'name'  => 'name',
            'label' => 'Name',
            'type'  => 'text',
        ]);

    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }


}