<?php 
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\PageCrudRequest as StoreRequest;
use App\Http\Requests\PageCrudRequest as UpdateRequest;
use App\Page;

class PageCrudController extends CrudController {

	public function __construct() {
        parent::__construct();
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel("App\Page");
        $this->crud->setRoute("admin/page");
        $this->crud->setEntityNameStrings('Page', 'page');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
         */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name'  => 'slug',
            'label' => "Slug",
        ]);

        $this->crud->addColumn([
            'name'  => 'title',
            'label' => "Title",
        ]);

        $this->crud->addColumn([
            'name'  => 'content',
            'label' => "Content",
        ]);
      

        // ------ CRUD FIELDS
        $this->crud->addField([ 
            'name'  => 'slug',
            'label' => 'Slug',
            'type'  => 'text',
        ]);

        $this->crud->addField([ 
            'name'  => 'title',
            'label' => 'Title',
            'type'  => 'text',
        ]);

        $this->crud->addField([ 
            'name'  => 'content',
            'label' => 'Content',
            'type'  => 'wysiwyg',
        ]);


    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }


}