<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Mail;
use Carbon\Carbon;
use App\User;
use App\Shop;
use App\ShippingCost;
use App\PrefectureShippingArea;
use App\ContentItem;
use App\Prefecture;
use App\Airport;
use App\Order;
use App\OutgoingEmail;
use App\DomesticShipping;
use App\DomesticShippingContentItem;
use App\OverseasShipping;
use App\OverseasShippingContentItem;

class EmailController extends Controller
{
    public function sentMails()
    {
        $mails = OutgoingEmail::whereNull('sent_at')->get();

        foreach ($mails as $mail) {
            Mail::send('mails.mail_template', ['content' => $mail->content], function ($message) use ($mail)
            {
                $message->from(env('MAIL_USERNAME'), 'Smart Delivery');

                $message->subject($mail->subject);
                $message->to($mail->destination_email);
                
                if ($mail->destination_email_cc != "" || $mail->destination_email_cc != NULL) {
                    $cc = explode(",", $mail->destination_email_cc);
                    $message->cc($cc);
                }

                if ($mail->destination_email_bcc != "" || $mail->destination_email_bcc != NULL) {
                    $bcc = explode(",", $mail->destination_email_bcc);
                    $message->bcc($bcc);
                }
            });

            if(count(Mail::failures()) > 0) 
            {
                foreach(Mail::failures as $email_address) {
                   // echo "$email_address <br />";
                    // echo "Failed to send mail ID: ".$mail->id."<br>";
                }
            } 
            else {
                $now = Carbon::now();
                OutgoingEmail::where('id', $mail->id)->update(['sent_at' => $now]);
                // echo "Sending mail ID: ".$mail->id." at ".$now."<br>";
            }
        }
    }

    public function sentMailById($id)
    {
        $mail = OutgoingEmail::where('id', $id)->first();

        if($mail) {
            Mail::send('mails.mail_template', ['content' => $mail->content], function ($message) use ($mail)
            {
                $message->from(env('MAIL_USERNAME'), 'Smart Delivery');

                $message->subject($mail->subject);
                $message->to($mail->destination_email);
                
                if ($mail->destination_email_cc != "" || $mail->destination_email_cc != NULL) {
                    $cc = explode(",", $mail->destination_email_cc);
                    $message->cc($cc);
                }

                if ($mail->destination_email_bcc != "" || $mail->destination_email_bcc != NULL) {
                    $bcc = explode(",", $mail->destination_email_bcc);
                    $message->bcc($bcc);
                }
            });

            if(count(Mail::failures()) > 0) 
            {
                foreach(Mail::failures as $email_address) {
                    // echo "Failed to send mail ID: ".$mail->id."<br>";
                }
            } 
            else {
                $now = Carbon::now();
                OutgoingEmail::where('id', $mail->id)->update(['sent_at' => $now]);
                // echo "Sending mail ID: ".$mail->id." at ".$now."<br>";
            }
        }
    }

    public function generateMailContentForAdmin($shipping_id, $type)
    {
        $order_date = "";
        $order_time = "";
        $shop_name = "";
        $customer_name = "";
        $order_amount = "";
        if ($type == "domestic") {
            $data = DomesticShipping::where('id', $shipping_id)->first();
        }
        else {
            $data = OverseasShipping::where('id', $shipping_id)->first();
        }

        if ($data) {
            $customer_name = $data->customer_name;
            $order_amount = $data->total_price;
            $datetime = explode(" ", $data->created_at);
            $order_date = $datetime[0];
            $order_time = $datetime[1];

            $shop = Shop::where('id', $data->shop_id)->first();
            if ($shop) {
                $shop_name = $shop->name;
            }

            $order_date = $order_date;
            $order_time = $order_time;
            $shop_name = $shop_name;
            $customer_name = $customer_name;
            $order_amount = $order_amount;
        }

        $txt = "SMARTDELIVERY事務局様、\n";
        $txt .= "お客様により配送注文の支払が完了しました。\n\n";

        $txt .= "受注年月日：".$order_date."\n";
        $txt .= "受注時刻：".$order_time."\n";
        $txt .= "受注店舗名：".$shop_name."\n";
        $txt .= "お客様名：".$customer_name."\n";
        $txt .= "受注金額：".number_format($order_amount)."円\n";

        return $txt;
    }

    public function generateMailContentForOwner($shipping_id, $type)
    {
        $order_date = "";
        $order_time = "";
        $shop_name = "";
        $customer_name = "";
        $order_amount = "";
        if ($type == "domestic") {
            $data = DomesticShipping::where('id', $shipping_id)->first();
        }
        else {
            $data = OverseasShipping::where('id', $shipping_id)->first();
        }

        if ($data) {
            $customer_name = $data->customer_name;
            $order_amount = $data->total_price;
            $datetime = explode(" ", $data->created_at);
            $order_date = $datetime[0];
            $order_time = $datetime[1];

            $shop = Shop::where('id', $data->shop_id)->first();
            if ($shop) {
                $shop_name = $shop->name;
            }

            $order_date = $order_date;
            $order_time = $order_time;
            $shop_name = $shop_name;
            $customer_name = $customer_name;
            $order_amount = $order_amount;
        }
        
        $txt = "SMARTDELIVERY事務局様、\n";
        $txt .= "お客様により配送注文の支払が完了しました。\n\n";

        $txt .= "受注年月日：".$order_date."\n";
        $txt .= "受注時刻：".$order_time."\n";
        $txt .= "受注店舗名：".$shop_name."\n";
        $txt .= "お客様名：".$customer_name."\n";
        $txt .= "受注金額：".number_format($order_amount)."円\n";

        return $txt;
    }

    public function generateMailContentForCustomer($shipping_id, $type)
    {
        $order_date = "";
        $order_time = "";
        $shop_name = "";
        $customer_name = "";
        $order_amount = "";
        if ($type == "domestic") {
            $data = DomesticShipping::where('id', $shipping_id)->first();
        }
        else {
            $data = OverseasShipping::where('id', $shipping_id)->first();
        }

        if ($data) {
            $customer_name = $data->customer_name;
            $order_amount = $data->total_price;
            $datetime = explode(" ", $data->created_at);
            $order_date = $datetime[0];
            $order_time = $datetime[1];

            $shop = Shop::where('id', $data->shop_id)->first();
            if ($shop) {
                $shop_name = $shop->name;
            }

            $order_date = $order_date;
            $order_time = $order_time;
            $shop_name = $shop_name;
            $customer_name = $customer_name;
            $order_amount = $order_amount;
        }
        
        $txt = $customer_name."様、\n";
        $txt .= "配送注文の支払が完了しました。\n\n";

        $txt .= "受注年月日：".$order_date."\n";
        $txt .= "受注時刻：".$order_time."\n";
        $txt .= "受注店舗名：".$shop_name."\n";
        $txt .= "お客様名：".$customer_name."\n";
        $txt .= "受注金額：".number_format($order_amount)."円\n";

        return $txt;
    }
}
