<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;

use Carbon\Carbon;
use App\User;
use App\Shop;
use Session;

class ShopController extends Controller
{
	public function indexSelectShop()
	{
		if (Session::has('shop_id')) {
            return redirect('/');
        }
		$data = array();
		$user = Auth::user();
    	$data['shops'] = Shop::where('user_id', $user->id)->get();
		return view('selectshop', $data);
	}

    public function getShop()
    {
        $data = array();
        $data['shop'] = Shop::get();
        return response()->json(['response'=>true, 'data' => $data['shop']]);
    }

    public function saveSelectedShop(Request $request)
    {
    	$response = array(
            "status" => "error",
            "message" => "",
            "data" => array(),
        );

        $input = $request->all();
        $shop_id = $input['shop_id'];
        Session::put('shop_id', $shop_id);

        if (Session::has('shop_id')) {
        	$response['status'] = 'success';
        }

        return $response;
    }

    public function addNewShop(Request $request)
    {
    	$response = array(
            "status" => "error",
            "message" => "",
            "data" => array(),
        );

        $input = $request->all();
        $shop_name = $input['shop_name'];
        $shop = new Shop;
        $shop->name = $shop_name;
        $shop->user_id = Auth::user()->id;
        $shop->save();

        Session::put('shop_id', $shop->id);

        if (Session::has('shop_id')) {
        	$response['status'] = 'success';
        }

        return $response;
    }
}
