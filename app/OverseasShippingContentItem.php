<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OverseasShippingContentItem extends Model
{
    use SoftDeletes;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'shipping_id',
        'content_item',
        'price',
        'piece'
    ];

    public function contentitem()
    {
    	return $this->hasOne('App\ContentItem', 'id', 'content_item');
    }
}
