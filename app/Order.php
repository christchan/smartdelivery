<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	use SoftDeletes;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'shipping_code',
        'order_date',
        'destination_address',
        'destination_tel',
        'shop_name',
        'customer_name',
        'customer_tel',
        'parcel_size',
        'parcel_weight',
        'packing_status',
        'contents',
        'total_shipping_cost',
        'items_grand_total',
        'stripe_charge_id',
        'shipping_type',
    ];
}
