<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class DomesticShipping extends Model
{
	use SoftDeletes;
    use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'shop_id',
        'record_id',
        'stripe_charge_id',
        'payment_type',
        'payment_method',
        'item_total_qty',
        'item_grand_total',
        'packing_status',
        'total_weight',
        'total_size',
        'total_price',
        'cool_type',
        'slip_id',
        'shipping_date',
        'delivery_date',
        'delivery_time_range',
        'delivery_code',
        'customer_tel',
        'customer_tel_edaban',
        'customer_post_code',
        'customer_address1',
        'customer_address2',
        'company_name',
        'company_division',
        'customer_name',
        'customer_email',
        'customer_name_kana',
        'customer_sir',
        'agent_code',
        'agent_tel',
        'agent_tel_edaban',
        'agent_post_code',
        'agent_address1',
        'agent_address2',
        'agent_name',
        'agent_name_kana',
        'item1_code',
        'item1_name',
        'item2_code',
        'item2_name',
        'item1_atsukai',
        'item2_atsukai',
        'remarks',
        'receipt_id',
        'collect_payment_amount',
        'collect_payment_tax',
        'eigyo_stop',
        'eigyo_code',
        'hakko_maisu',
        'kosu_flag',
        'others1',
        'others2',
        'others3',
        'others4',
        'others5',
        'others6',
        'others7',
        'others8',
        'others9',
        'others10',
        'others11',
        'others12',
        'others13',
        'others14',
        'others15',
        'others16',
        'others17',
        'others18',
        'others19',
        'others20',
        'others21',
        'others22',
        'others23',
        'others24',
        'others25',
        'others26',
        'others27',
        'others28',
        'others29',
        'others30',
        'others31',
        'others32',
        'others33',
        'others34',
    ];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function shop()
    {
        return $this->hasOne('App\Shop','id','shop_id');
    }

    public function contents()
    {
        return $this->hasMany('App\DomesticShippingContentItem','shipping_id','id');
    }
}
