<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Prefecture extends Model
{
    use SoftDeletes;
    use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'lang_id',
    ];

    public function languages()
    {
        return $this->hasOne('App\Language','id','lang_id');
    }

    public function prefectureshippingarea()
    {
        return $this->belongsTo('App\PrefectureShippingArea','id','pref_id');
    }
}
