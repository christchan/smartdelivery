<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class OverseasShipping extends Model
{
	use SoftDeletes;
    use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'shop_id',
        'record_id',
        'stripe_charge_id',
        'shipping_way_code',
        'shipping_company',
        'payment_method',
        'item_total_qty',
        'item_grand_total',
        'packing_status',
        'shipping_date',
        'delivery_date',
        'customer_name',
        'customer_email',
        'customer_other_address',
        'customer_other_address2',
        'customer_city',
        'customer_state',
        'customer_post_code',
        'customer_tel',
        'customer_country',
        'selected_continent',
        'remarks',
        'shipping_company_code',
        'product_type',
        'total_price',
        'total_weight',
        'total_size',
        'item1_code',
        'item1_name',
        'item1_hs_code',
        'item1_made_country_code',
        'item1_pieces',
        'item1_unit_price',
        'item1_weight',
        'item2_code',
        'item2_name',
        'item2_hs_code',
        'item2_made_country_code',
        'item2_pieces',
        'item2_unit_price',
        'item2_weight',
        'item3_code',
        'item3_name',
        'item3_hs_code',
        'item3_made_country_code',
        'item3_pieces',
        'item3_unit_price',
        'item3_weight',
    ];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function shop()
    {
        return $this->hasOne('App\Shop','id','shop_id');
    }

    public function contents()
    {
        return $this->hasMany('App\OverseasShippingContentItem','shipping_id','id');
    }
}
