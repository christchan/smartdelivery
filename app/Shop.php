<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Shop extends Model
{
	use SoftDeletes;
	use CrudTrait;
	
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
